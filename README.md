# CS 373 IDB

## Authors and Acknowledgment
Davie, Sumedh, Nghia, Canyon, Angela, Jordan (The Dream Team!)

## Project Name
HTMelodies

## Description
HTMelodies is a website connecting information about songs, albums, and artists. The website aggregates this information and presents it in a convenient, aesthetically pleasing way for users to consume.
Users will be able to learn about songs, albums, and artists they are already interested in by using the search tool. The search feature will allow users to filter for characteristics of songs, albums, and artists that they find appealing, such as genre and popularity.
HTMelodies also encourages users to explore new content. Each page links to other pages with related content. Each of the songs, albums, and artists' homepages will display a collection of the most popular songs that users can learn about.

## Makefile Instructions
Each makefile command can be run individually (i.e. make command).
However, grouping makefile commands can be more efficient and fun.

The 6 primary makefile commands are:
1. make
2. make clean
3. make check
4. make set-backend-api
5. make test-all
6. make run-all

### make
Calls the other five commands below in order (clean, check, set-backend-api, test-all, and run-all).

### make clean
Removes temporary and log files, node_modules and build folders, html documentation, and test output.
Calls other make commands including clean-frontend, clean-backend, clean-postman.

### make check
Checks that the required files are in your working directory. Required filenames are defined at the top of the makefile using the variable CFILES.

### make set-backend-api
To avoid constantly changing our API url and database password between local testing and GCP deployment, this command creates two files called Global.js located at frontend/src/components/ and Global.py located at backend/. Global.js contains the constant API_URL and Global.py contains the constant DB_PASS.

This command has two optional command-line variables:
1. env
    - Specifies the execution environment
    - Possible values: gcp, local
    - Default: gcp
2. pass
    - Specifies YOUR LOCAL database password
    - Possible values: anything
    - Default: empty string

| Example                                                 | Make command executed      | Result                                                                  |
| ------------------------------------------------------- | -------------------------- | ----------------------------------------------------------------------- |
| make set-backend-api                                    | make set-backend-api-gcp   |  API_URL='https://cs373-idb-backend.uc.r.appspot.com', DB_PASS='csuser' |
| make set-backend-api env=gcp                            | make set-backend-api-gcp   |  API_URL='https://cs373-idb-backend.uc.r.appspot.com', DB_PASS='csuser' |
| make set-backend-api env=local                          | make set-backend-api-local |  API_URL='', DB_PASS=''                                                 |
| make set-backend-api env=local pass=YOUR_LOCAL_PASSWORD | make set-backend-api-local |  API_URL='', DB_PASS='YOUR_LOCAL_PASSWORD'                              |

### make test-all
Executes tests for database, API, and Postman.
Calls other make commands including unit-tests (which consists of the commands test-db and test-api) and postman-tests. The make commands test-db, test-api, and postman-tests execute the files db_tests.py, api_tests.py, and htmelodies_api.json, respectively.

### make run-all
Creates a log file of project commits, installs requirements found in requirements.txt, and executes the backend and frontend simultaneously.
Calls other make commands including log, install-requirements, run-backend, and run-frontend. The command run-backend formats Python files using Autopep8, creates html documentation using Pydoc, and executes main.py. The command run-frontend installs the node_modules and build folders then runs npm start. 
