.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# Required files
CFILES :=                                 \
	.gitignore                            \
	.gitlab-ci.yml						  \
	package.json                          \
	frontend/app.yaml                     \
	frontend/requirements.txt             \
	frontend/package.json                 \
	backend/app.yaml                      \
	backend/requirements.txt              \
	backend/.flaskenv                     \
	backend/api_tests.py                  \
	backend/data_api.py                   \
	backend/db_secrets.py                 \
	backend/db_tests.py                   \
	backend/db.py                         \
	backend/main.py                       \
	backend/models.py                     \
	backend/refresh.py                    \
	Postman_Collections/htmelodies_api.json

# Determine which tool to use based on machine
ifeq ($(shell uname), Darwin)          # Apple
	PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python3 -m pydoc
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

# make versions: See versions for each tool
versions:
	which        $(PYTHON)
	$(PYTHON)    --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version


#####################################################################
#
#  make clean: clean-frontend, clean-backend, clean-postman
#
#####################################################################

# make clean-frontend: Remove node_modules and build
clean-frontend:
	rm -rf frontend/node_modules
	rm -rf frontend/build

# make clean-backend: Remove temporary files, html documentation, and test output
clean-backend:
	rm -rf backend/__pycache__
	rm -f  backend/api_tests.html
	rm -f  backend/db_secrets.html
	rm -f  backend/db_tests.html
	rm -f  backend/db.html
	rm -f  backend/main.html
	rm -f  backend/models.html
	rm -f  backend/refresh.html
	rm -f  backend/.coverage
	rm -f  backend/db_tests_output.txt
	rm -f  backend/api_tests_output.txt

# make clean-postman: Remove test output files
clean-postman:
	rm -f Postman_Collections/postman_tests_output.txt

# make clean: Remove temporary and log files, node_modules, build, documentation, and test output
clean:
	@echo remove tmp
	rm -f  *.tmp
	@echo remove pycache
	rm -rf __pycache__
	@echo remove IDB3.log
	rm -f  IDB3.log
	make clean-frontend
	make clean-backend
	make clean-postman


#####################################################################
#
#  make check
#
#####################################################################

# make check: Check the existence of required files
check:
	@not_found=0;                                 \
	for i in $(CFILES);                           \
    do                                            \
    	if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";


#####################################################################
#
#  make set-backend-api: set-backend-api-gcp OR set-backend-api-local
#
#####################################################################

# make set-backend-api-gcp
# Sets API_URL to https://cs373-idb-backend.uc.r.appspot.com
# Sets DB_PASS to "csuser"
set-backend-api-gcp:
	cd frontend/src/components/ && touch Global.js
	cd frontend/src/components/ && echo "const API_URL = 'https://cs373-idb-backend.uc.r.appspot.com';" > Global.js
	cd frontend/src/components/ && echo "export { API_URL };" >> Global.js
	cd backend/ && touch Global.py
	cd backend/ && echo "DB_PASS = 'csuser'" > Global.py

# make set-backend-api-local pass=YOUR_LOCAL_PASSWORD
# Sets API_URL to empty string
# Sets DB_PASS to user given input
set-backend-api-local:
	cd frontend/src/components/ && touch Global.js
	cd frontend/src/components/ && echo "const API_URL = '';" > Global.js
	cd frontend/src/components/ && echo "export { API_URL };" >> Global.js
	cd backend/ && touch Global.py
	cd backend/ && echo "DB_PASS = '$(pass)'" > Global.py

# make set-backend-api env=ENV pass=YOUR_LOCAL_PASSWORD
# Sets API_URL in frontend/src/components/Global.js (used in components)
# Sets DB_PASS in backend/Global.py 				(used in models.py)
# To run 	set-backend-api-gcp 	set env=gcp
# To run 	set-backend-api-local 	set env=local pass=YOUR_LOCAL_PASSWORD
# Default 	set-backend-api-gcp
set-backend-api-:
	make set-backend-api-gcp

set-backend-api:
	make set-backend-api-$(env)


#####################################################################
#
#  make test-all: unit-tests (test-db and test-api), postman-tests
#
#####################################################################

# make test-db: Runs coverage on db_tests.py and coverage report on db.py
test-db:
	cd backend/ && $(COVERAGE) run db_tests.py > db_tests_output.txt 2>&1
	cd backend/ && $(COVERAGE) report -m db.py >> db_tests_output.txt
	rm -f  backend/.coverage

# make test-api: Runs coverage on api_tests.py and coverage report on main.py
test-api:
	cd backend/ && $(COVERAGE) run api_tests.py > api_tests_output.txt 2>&1
	cd backend/ && $(COVERAGE) report -m main.py >> api_tests_output.txt
	rm -f  backend/.coverage

# make unit-tests: Executes db_tests.py and api_tests.py (leading "-" lets makefile continue on error)
unit-tests:
	-make test-db
	-make test-api

# make postman-tests: Executes htmelodies_api.json
postman-tests:
	cd Postman_Collections/ && newman run htmelodies_api.json > postman_tests_output.txt

# make test-all: Executes tests for database, API, and Postman (leading "-" lets makefile continue on error)
test-all:
	-make unit-tests
	-make postman-tests


#####################################################################
#
#  make run-all: log, install-requirements, 
#                run-backend  (format and documentation), 
#                run-frontend (install-frontend and build-frontend)
#
#####################################################################

# make log: Calls git log to track all project commits
log:
	git log > IDB3.log

# make install-requirements: Install dependencies found in requirements.txt
install-requirements:
	$(PIP) install -r backend/requirements.txt
	$(PIP) install -r frontend/requirements.txt

# make format: Calls AUTOPEP8 on backend files
format:
	cd backend/ && $(AUTOPEP8) -i api_tests.py
	cd backend/ && $(AUTOPEP8) -i data_api.py
	cd backend/ && $(AUTOPEP8) -i db_secrets.py
	cd backend/ && $(AUTOPEP8) -i db_tests.py
	cd backend/ && $(AUTOPEP8) -i db.py
	cd backend/ && $(AUTOPEP8) -i main.py
	cd backend/ && $(AUTOPEP8) -i models.py
	cd backend/ && $(AUTOPEP8) -i refresh.py

# make documentation: Calls PYDOC on backend files (except backend/data_api.py throws error, from asyncio.windows_events import NULL)
documentation: backend/api_tests.py backend/db_secrets.py backend/db_tests.py backend/db.py backend/main.py backend/models.py backend/refresh.py
	cd backend/ && $(PYDOC) -w api_tests 	> api_tests.html	1> /dev/null
	cd backend/ && $(PYDOC) -w db_secrets 	> db_secrets.html	1> /dev/null
	cd backend/ && $(PYDOC) -w db_tests 	> db_tests.html		1> /dev/null
	cd backend/ && $(PYDOC) -w db 			> db.html			1> /dev/null
	cd backend/ && $(PYDOC) -w main 		> main.html			1> /dev/null
	cd backend/ && $(PYDOC) -w models 		> models.html		1> /dev/null
	cd backend/ && $(PYDOC) -w refresh 		> refresh.html		1> /dev/null

# make run-backend: Format python files, recreate documentation, then execute main
run-backend:
	make format
	make documentation
	cd backend/ && $(PYTHON) main.py

# make install-frontend: Install node_modules folder
install-frontend:
	cd frontend/ && npm install

# make build-frontend: Install build folder
build-frontend:
	cd frontend/ && npm run build

# make run-frontend: Reinstall node_modules and build folders, then run npm start
run-frontend:
	make install-frontend
	make build-frontend
	cd frontend/ && npm start

# make run-all: Executes backend then frontend
run-all:
	make log
	make install-requirements
	make run-backend & make run-frontend


#####################################################################
#
#  make env=ENV pass=YOUR_LOCAL_PASSWORD
#
#####################################################################

all: clean check set-backend-api test-all run-all
