import './App.css';

// Import Splash page
import Splash from './components/Splash/Splash';

// Called by index.js
function App() {
  return (
    <div>
      <div className="App">
        <Splash></Splash>
      </div>
    </div>
  );
}

export default App;
