// Import react router for navigation
import { BrowserRouter, Routes, Route } from 'react-router-dom';

// Import pages
import App from './App';
import GlobalSearch from './components/Search/GlobalSearch';
import About from './components/About/About';
import Output from './components/Output/Output';
import Songs from './components/Song/Songs';
import Albums from './components/Album/Albums';
import Artists from './components/Artist/Artists';
import Song from './components/Song/Song';
import Album from './components/Album/Album';
import Artist from './components/Artist/Artist';
import Visualizations from './components/Visualization/Visualizations';
import OtherVisualizations from './components/Visualization/OtherVisualizations';

const RouteSwitch = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<App />}></Route>
                <Route path="/search" element={<GlobalSearch />}></Route>
                <Route path="/about" element={<About/>}></Route>
                <Route path="/output/:filename" element={<Output/>}></Route>
                <Route path="/songs" element={<Songs/>}></Route>
                <Route path="/albums" element={<Albums/>}></Route>
                <Route path="/artists" element={<Artists/>}></Route>
                <Route path="/song/:id" element={<Song/>}></Route>
                <Route path="/album/:id" element={<Album/>}></Route>
                <Route path="/artist/:id" element={<Artist/>}></Route>
                <Route path="/visualizations" element={<Visualizations/>}></Route>
                <Route path="/othervisualizations" element={<OtherVisualizations/>}></Route>
            </Routes>
        </BrowserRouter>
    );
}

export default RouteSwitch;
