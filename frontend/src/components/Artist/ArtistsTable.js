import { useEffect } from "react";

// Import react router for navigation
import { Link } from "react-router-dom";

// Table on Artists page
const ArtistsTable = (artistsTable) => {

    // Import script to sort table
    useEffect(() => {
        const script = document.createElement("script")
        script.src = "https://www.kryogenix.org/code/browser/sorttable/sorttable.js"
        script.async = true
        document.body.appendChild(script)

        return () => {
            document.body.removeChild(script)
        }
    })

    return (
        <div>
            <div className="bg-black" style={{height: "2vh"}}></div>
            <div className="shadow-4 rounded-1 overflow-hidden p-1">
                <table className="table align-middle mb-0 bg-white sortable table-hover" width={"100%"}>
                    <thead className="bg-light">
                        <tr>
                            {artistsTable.headers.map((title) => {
                                return (
                                    <th style={{cursor: "pointer"}}>{title}</th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {artistsTable.artists.map((artist, index) => {
                            return (
                                <tr className="item">
                                    
                                    {/* Rank: popularity */}
                                    <td>{artist["popularity"]}</td>

                                    {/* Artist: artist_id, artist_name, image_url */}
                                    <td>
                                        <Link to={"/artist/" + artist["artist_id"]} className="list-group-item list-group-item-action">
                                            <div class = "d-flex align-items-center">
                                                <img src={artist["image_url"]} alt="" style={{width: "45px", height: "45px"}} className="rounded-circle"></img>
                                                            
                                                <div className="ms-3">
                                                    <h6 className="mb-1 mt-1">{artist["artist_name"]}</h6>
                                                </div>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Genres: genre */}
                                    <td>
                                        {artist["genre"].map((genre) => {
                                            return (
                                                <p className="mb-1 mt-1">{genre}</p>
                                            )
                                        })}
                                    </td>

                                    {/* Monthly Listeners: follower_count */}
                                    <td>{artist["follower_count"]}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default ArtistsTable;
