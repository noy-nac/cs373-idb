import axios from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import ArtistCards from './ArtistCards';

// Artist details page
const Artist = (artist) => {

    const [artistData, setArtistData] = useState({})

    const { id } = useParams();

    // Get data from backend
    useEffect(() => {
        axios.get(API_URL + '/artist/' + id)
            .then(res => {
                setArtistData(res.data)
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>

            {/* Set background to artist name, number of followers, and picture */}
            <BackgroundGradient title={artistData["artist_name"]} 
                subtitle={artistData["follower_count"] + " monthly listeners"} 
                pic={artistData["image_url"]}>
            </BackgroundGradient>

            {/* Set cards to genres and popularity */}
            <ArtistCards artistData={artistData}></ArtistCards>
        </div>
    );
}

export default Artist;
