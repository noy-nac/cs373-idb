import React from 'react';

class ArtistSearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            artistText: "",
            genreText: "",
            rankMin: "",
            rankMax: "",
            monthlyMin: "",
            monthlyMax: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <form>
                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Search by Attribute</legend>

                    {/* ARTIST NAME */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="artistTextInput">Artist Name</label>
                        <div className="input-group">
                            <div className="input-group-text">Name</div>
                            <input
                                type="text"
                                className="form-control"
                                id="artistTextInput"
                                name="artistText"
                                placeholder="Artist Name"
                                value={this.state.artistText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* SONG TITLE */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="genreTextInput">Artist Genre</label>
                        <div className="input-group">
                            <div className="input-group-text">Genre</div>
                            <input
                                type="text"
                                className="form-control"
                                id="genreTextInput"
                                name="genreText"
                                placeholder="Artist Genre"
                                value={this.state.genreText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                    
                </fieldset>
                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Popularity Rank</legend>

                    {/* Released From */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="rankFromInput">Rank Min</label>
                        <div className="input-group">
                            <div className="input-group-text">Min</div>
                            <input
                                type="number"
                                className="form-control"
                                id="rankMinInput"
                                name="rankMin"
                                value={this.state.rankMin}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* Released To */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="rankToInput">Rank Max</label>
                        <div className="input-group">
                            <div className="input-group-text">Max</div>
                            <input
                                type="number"
                                className="form-control"
                                id="rankMaxInput"
                                name="rankMax"
                                value={this.state.rankMax}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                </fieldset>

                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Monthly Listeners</legend>


                    {/* Duration Min */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="monthlyMinInput">Min Monthly Listeners</label>
                        <div className="input-group">
                            <div className="input-group-text">Min</div>
                            <input
                                type="number"
                                className="form-control"
                                id="monthlyMinInput"
                                name="monthlyMin"
                                value={this.state.monthlyMin}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* Duration Max */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="monthlyMaxInput">Max Monthly Listeners</label>
                        <div className="input-group">
                            <div className="input-group-text">Max</div>
                            <input
                                type="number"
                                className="form-control"
                                id="monthlyMaxInput"
                                name="monthlyMax"
                                value={this.state.monthlyMax}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                </fieldset>
                
                <div class="col-auto ">
                    <button type="submit" class="btn btn-dark my-2">Search</button>
                </div>
            </form>
        );
    }
}

export default ArtistSearchForm;