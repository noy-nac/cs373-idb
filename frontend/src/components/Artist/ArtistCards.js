// Cards that display a particular artist's details on the Artist page
const ArtistCards = (artistCards) => {
    return (
        <div>
            <div className="bg-white" style={{height: "5vh"}}></div>
            <div className="d-flex align-items-center">
                <div className="container-fluid">
                    <div className="row">

                        {/* Genres */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>Genres</strong></h5>
                                    <div className="container-fluid">
                                        <div className="row">

                                            {/* MUST have ? to avoid map error */}
                                            {(artistCards.artistData["genre"])?.map((genre) => {
                                                return (
                                                    <div className="col p-2">
                                                        <div className="btn btn-outline-light">{genre}</div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Popularity */}
                        <div className="col my-1" style={{height: "20vh"}}>
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{artistCards.artistData["popularity"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Rank</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ArtistCards;
