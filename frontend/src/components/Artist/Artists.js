import axios from 'axios'
import {useState, useEffect} from 'react'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundCarousel from '../Background/BackgroundCarousel';
import LocalSearch from '../Search/LocalSearch';
import ArtistsTable from './ArtistsTable';
import Pagination from '../Paginate/pagination';

// Artists page
const Artists = (artists) => {

    const [artistsData, setArtistsData] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage] = useState(10)

    // Get data from backend (artist_data.json)
    useEffect(() => {
        axios.get(API_URL + '/artists/' + window.location.search)
            .then(res => {
                setArtistsData(res.data["Artists"])
            })
            .catch(err => console.log(err))
    }, [])

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = artistsData.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = pageNumber => setCurrentPage(pageNumber)

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>
            <BackgroundCarousel title={"Discover Artists"} 
                pics={[
                    "https://wallpapercave.com/wp/wp7172132.jpg",
                    "https://images.unsplash.com/photo-1583795310794-4168d4ff1be2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                ]}>
            </BackgroundCarousel>
            <LocalSearch formType="artist" />
            <ArtistsTable artists={currentPosts} headers={["Rank", "Artist", "Genres", "Monthly Listeners"]}></ArtistsTable>
            <Pagination
                postsPerPage={postsPerPage}
                totalPosts={artistsData.length}
                paginate={paginate}
                currentPage={currentPage}
            />
        </div>
    );
}

export default Artists;
