import DeveloperAccordion from "./DeveloperAccordion";

// Card for each developer's name, photo, and Accordion on About page
// Gives data to DeveloperAccordion
const DeveloperCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Name */}
                <h5 className="card-title">{card.data["Name"]}</h5>
                <h6 className="card-subtitle mb-2 text-secondary">Developer</h6>
                
                {/* Photo */}
                <div style={{display:"block", marginLeft: "auto", marginRight: "auto", width: "50%"}}>
                    <img className="rounded-circle mb-3" src={`../static/images/${card.data["Photo"]}`} alt="Developer Photo" width={150} height={150} style={{overflow: "hidden"}}></img>
                </div>

                {/* Accordion */}
                <DeveloperAccordion index={card.index} data={card.data}></DeveloperAccordion>
            </div>
        </div>
    );
}

export default DeveloperCard;
