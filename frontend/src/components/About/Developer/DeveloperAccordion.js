// Accordion for each developer's bio, responsibilities, and stats on About page
// Given data from DeveloperCard
const DeveloperAccordion = (accord) => {
    return (
        <div className="accordion accordion-flush" id={"accordionFlushDevelopers" + accord.index}>

            {/* Bio */}
            <div className="accordion-item">
                <h2 className="accordion-header" id={"flush-headingOne" + accord.index}>
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#flush-collapseOne" + accord.index} aria-expanded="false" aria-controls={"flush-collapseOne" + accord.index}>
                        <strong>Bio</strong>
                    </button>
                </h2>
                <div id={"flush-collapseOne" + accord.index} className="accordion-collapse collapse" aria-labelledby={"flush-headingOne" + accord.index} data-bs-parent={"#accordionFlushDevelopers" + accord.index}>
                <div className="accordion-body">
                    {accord.data["Bio"].map((paragraph) => {
                        return (
                            <div className="row">
                                <p>{paragraph}</p>
                            </div>
                        )
                    })}
                </div>
                </div>
            </div>

            {/* Responsibilities */}
            <div className="accordion-item">
                <h2 className="accordion-header" id={"flush-headingTwo" + accord.index}>
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#flush-collapseTwo" + accord.index} aria-expanded="false" aria-controls={"flush-collapseTwo" + accord.index}>
                        <strong>Responsibilities</strong>
                    </button>
                </h2>
                <div id={"flush-collapseTwo" + accord.index} className="accordion-collapse collapse" aria-labelledby={"flush-headingTwo" + accord.index} data-bs-parent={"#accordionFlushDevelopers" + accord.index}>
                <div className="accordion-body">
                    <ul>
                        {accord.data["Responsibilities"].map((resp, index) => {
                            return (
                                <li>{resp}</li>
                            )
                        })}
                    </ul>
                </div>
                </div>
            </div>

            {/* Stats */}
            <div className="accordion-item">
                <h2 className="accordion-header" id={"flush-headingThree" + accord.index}>
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#flush-collapseThree" + accord.index} aria-expanded="false" aria-controls={"flush-collapseThree" + accord.index}>
                        <strong>Stats</strong>
                    </button>
                </h2>
                <div id={"flush-collapseThree" + accord.index} className="accordion-collapse collapse" aria-labelledby={"flush-headingThree" + accord.index} data-bs-parent={"#accordionFlushDevelopers" + accord.index}>
                <div className="accordion-body">
                    <p><strong>Phase I Commits</strong><span style={{float: "right"}}>{accord.data["Commits"]["PhaseI"]}</span></p>
                    <p><strong>Phase II Commits</strong><span style={{ float: "right" }}>{accord.data["Commits"]["PhaseII"]}</span></p>
                    <p><strong>Phase III Commits</strong><span style={{ float: "right" }}>{accord.data["Commits"]["PhaseIII"]}</span></p>
                    <p><strong>Total Issues</strong><span style={{ float: "right" }}>{accord.data["Issues"]}</span></p>
                    <p><strong>Total Tests</strong><span style={{float: "right"}}>{accord.data["Tests"]}</span></p>
                </div>
            </div>
            </div>
        </div>
    );
}

export default DeveloperAccordion;
