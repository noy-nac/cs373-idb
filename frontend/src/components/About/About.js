import axios from 'axios'
import {useState, useEffect} from 'react'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import DeveloperCard from './Developer/DeveloperCard';
import ProjectStats from './Project/ProjectStats';
import ProjectData from './Project/ProjectData';
import ProjectTools from './Project/ProjectTools';
import ProjectTests from './Project/ProjectTests';

// About page
const About = (about) => {

    const [website, setWebsite] = useState("")
    const [developers, setDevelopers] = useState([])
    const [statsSectionOne, setStatsSectionOne] = useState([])
    const [statsSectionTwo, setStatsSectionTwo] = useState([])
    const [dataSectionOne, setDataSectionOne] = useState([])
    const [dataSectionTwo, setDataSectionTwo] = useState([])
    const [toolsSectionOne, setToolsSectionOne] = useState([])
    const [toolsSectionTwo, setToolsSectionTwo] = useState([])
    const [UMLDiagram, setUMLDiagram] = useState("")
    const [databaseTests, setDatabaseTests] = useState({})
    const [APITests, setAPITests] = useState({})

    // Get data from backend (about.json)
    useEffect(() => {
        axios.get(API_URL + '/about/')
            .then(res => {
                setWebsite(res.data["Website"])
                setDevelopers(res.data["Developers"])
                setStatsSectionOne(res.data["StatsSectionOne"])
                setStatsSectionTwo(res.data["StatsSectionTwo"])
                setDataSectionOne(res.data["DataSectionOne"])
                setDataSectionTwo(res.data["DataSectionTwo"])
                setToolsSectionOne(res.data["ToolsSectionOne"])
                setToolsSectionTwo(res.data["ToolsSectionTwo"])
                setUMLDiagram(res.data["UMLDiagram"])
                setDatabaseTests(res.data["DatabaseTests"])
                setAPITests(res.data["APITests"])
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>
            <BackgroundGradient title={"About Us"} subtitle={"Meet Our Team"} pic={`../static/images/musiclogo.png`}></BackgroundGradient>

            {/* Card and accordion for each developer's information */}
            <div className="container-fluid mt-5 mb-5">
                <div className="row">
                    {developers.map((data, index) => {
                        return (
                            // Large and medium windows: 3 people per row, Small window: 1 person per row
                            <div className="col-lg-4 col-md-4 col-sm-12 p-2">
                                <DeveloperCard index={index} data={data}></DeveloperCard>
                            </div>
                        )
                    })}
                </div>
            </div>

            {/* Project stats, data, tools, and tests */}
            <ProjectStats sectionOne={statsSectionOne} sectionTwo={statsSectionTwo}></ProjectStats>
            <ProjectData sectionOne={dataSectionOne} sectionTwo={dataSectionTwo}></ProjectData>
            <ProjectTools sectionOne={toolsSectionOne} sectionTwo={toolsSectionTwo}></ProjectTools>
            <ProjectTests UMLDiagram={UMLDiagram} databaseTests={databaseTests} APITests={APITests}></ProjectTests>
        </div>
    );
}

export default About;
