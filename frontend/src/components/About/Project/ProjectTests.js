// Import react router for navigation
import { Link } from "react-router-dom";

// Section for UML and tests on About page
const ProjectTests = (projTests) => {
    return (
        <div className="container-fluid mt-5 mb-5">
            <div className="row"><h1>Project Diagrams and Tests</h1></div>
            <div className="row">
                {/* UML Diagram */}
                <div className="col-lg-6 col-md-6 col-sm-12 p-2">
                    <div className="card" style={{backgroundColor: "black"}}>
                        <div className="card-body" style={{color: "white"}}>
                            <h5 className="card-title mb-2"><strong>UML Diagram</strong></h5>
                            <div className="row p-2">
                                <img src={`../static/images/${projTests.UMLDiagram}`} alt="" height={740}></img>
                            </div>
                         </div>
                    </div>
                </div>
                {/* Tests */}
                <div className="col-lg-6 col-md-6 col-sm-12 p-2">
                    <div className="card" style={{backgroundColor: "black"}}>
                        <div className="card-body" style={{color: "white"}}>
                            <h5 className="card-title mb-2"><strong>Unittests</strong></h5>

                            {/* Database tests */}
                            <div className="row p-2">
                                <h5 className="mb-2">Database</h5>
                                <img src={`../static/images/${projTests.databaseTests["Photo"]}`} alt="" height={275}></img>
                                <div className="p-2" style={{alignItems: "center", justifyContent: "center", display: "flex"}}>
                                    <Link to={"/output/" + projTests.databaseTests["Test"]} className="btn btn-outline-light">
                                        Run {projTests.databaseTests["Test"]}.py
                                    </Link>
                                </div>
                            </div>
                            
                            {/* API tests */}
                            <div className="row p-2">
                                <h5 className="mb-2">API</h5>
                                <img src={`../static/images/${projTests.APITests["Photo"]}`} alt="" height={275}></img>
                                <div className="p-2" style={{alignItems: "center", justifyContent: "center", display: "flex"}}>
                                    <Link to={"/output/" + projTests.APITests["Test"]} className="btn btn-outline-light">
                                        Run {projTests.APITests["Test"]}.py
                                    </Link>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProjectTests;
