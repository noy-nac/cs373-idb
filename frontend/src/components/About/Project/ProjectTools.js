// Section for project tools on About page
const ProjectTools = (projTools) => {
    return (
        <div className="container-fluid mt-5 mb-5">
            <div className="row"><h1>Project Tools</h1></div>
            <div className="row">
                {/* Links to frontend and backend tools */}
                {(projTools.sectionOne).map((tools) => {
                    return (
                        <div className="col-lg-6 col-md-6 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title mb-2">{tools["Title"]}</h5>
                                    <ul>
                                        {(tools["Data"]).map((toolsDetails) => {
                                            return (
                                                <li><a href={toolsDetails["Link"]} target="_blank">{toolsDetails["Subtitle"]}</a></li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    )
                })}
                {/* Links to other tools */}
                {(projTools.sectionTwo).map((tools) => {
                    return (
                        <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title mb-2">{tools["Title"]}</h5>
                                    <div className="row">
                                        <div className="col-lg-6 col-md-6 col-sm-12">
                                            <ul>
                                                {(tools["DataColumnOne"]).map((toolsDetails) => {
                                                    return (
                                                        <li><a href={toolsDetails["Link"]} target="_blank">{toolsDetails["Subtitle"]}</a></li>
                                                    )
                                                })}
                                            </ul>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12">
                                            <ul>
                                                {(tools["DataColumnTwo"]).map((toolsDetails) => {
                                                    return (
                                                        <li><a href={toolsDetails["Link"]} target="_blank">{toolsDetails["Subtitle"]}</a></li>
                                                    )
                                                })}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default ProjectTools;
