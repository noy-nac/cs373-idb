// Section for project stats on About page
const ProjectStats = (projStats) => {
    return (
        <div className="container-fluid mt-5 mb-5">
            <div className="row"><h1>Project Stats</h1></div>
            <div className="row">
                {/* Total commits, issues, and tests */}
                {(projStats.sectionOne).map((stat) => {
                    return (
                        <div className="col-lg-4 col-md-4 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title">{stat["Title"]}</h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">{stat["Subtitle"]}</h6>
                                </div>
                            </div>
                        </div>
                    )
                })}
                {/* Links to Postman, GitLab, and Speaker Deck */}
                {(projStats.sectionTwo).map((stat) => {
                    return (
                        <div className="col-lg-6 col-md-6 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title mb-2">{stat["Title"]}</h5>
                                    <a href={stat["Link"]} target="_blank" className="btn btn-outline-light mb-2">{stat["Subtitle"]}</a>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default ProjectStats;
