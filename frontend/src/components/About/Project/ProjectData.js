// Section for project data on About page
const ProjectData = (projData) => {
    return (
        <div className="container-fluid mt-5 mb-5">
            <div className="row"><h1>Project Data</h1></div>
            <div className="row">
                {/* Links to sources and Postman */}
                {(projData.sectionOne).map((data) => {
                    return (
                        <div className="col-lg-6 col-md-6 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title mb-2">{data["Title"]}</h5>
                                    <div className="row">
                                        {(data["Data"]).map((dataDetails) => {
                                            return (
                                                <div className="col-lg-4 col-md-4 col-sm-12 mb-2">
                                                    <a href={dataDetails["Link"]} target="_blank" className="btn btn-outline-light">{dataDetails["Subtitle"]}</a>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
                {/* Descriptions of data scraping */}
                {(projData.sectionTwo).map((data) => {
                    return (
                        <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                            <div className="card" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title mb-2">{data["Title"]}</h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">{data["Subtitle"]}</h6>
                                    <ul>
                                        {(data["Data"]).map((desc) => {
                                            return (
                                                <li>{desc}</li>
                                            )
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default ProjectData;
