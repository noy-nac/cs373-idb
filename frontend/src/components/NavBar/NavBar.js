// Import react router for navigation
import { Link } from "react-router-dom";

// NavBar on every page
const NavBar = (navBar) => {
    return (
        <nav className="navbar sticky-top navbar-dark navbar-expand-lg" style={{backgroundColor: "black"}}>
            <div className="container-fluid my-2">
                <a href="/">
                    <img src={navBar.pic} alt="Logo" height="50" width="50" className="d-inline-block align-text-top ms-5"></img>
                </a>
                <span className="navbar-brand text-white mb-0 ms-2 h1">
                    <a className="text-decoration-none text-white" href="/"><strong>{navBar.title}</strong></a>
                </span>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav ms-auto me-5">

                        {/* Set menu items to page names */}
                        {navBar.pages.map((page) => {
                            return (
                                <Link to={page["Path"]} className="nav-link text-white"><strong>{page["Title"]}</strong></Link>
                            )
                        })}
                        {/* Search */}
                        <div className="nav-link text-white active"><strong>|</strong></div>
                        <a className="nav-link" href="/search/" style={{color: "rgba(203, 5, 203, 0.863)"}}><strong>Search</strong></a>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default NavBar;