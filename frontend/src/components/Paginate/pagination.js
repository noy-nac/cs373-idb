import React, { useEffect } from "react";

const Pagination = (props) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(props.totalPosts / props.postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav>
      <br/>
      <ul className='pagination justify-content-center'>
        {pageNumbers.map(number => (
            <li key={number} className={ props.currentPage == number ? 'page-item active' : 'page-item'}>
            <a onClick={() => props.paginate(number)} className='page-link'>
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;