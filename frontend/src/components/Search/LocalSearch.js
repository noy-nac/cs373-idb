import React from 'react';
import useCollapse from 'react-collapsed';
import SongSearchForm from '../Song/SongSearchForm';
import AlbumSearchForm from '../Album/AlbumSearchForm';
import ArtistSearchForm from '../Artist/ArtistSearchForm';

const LocalSearch = (localSearch) => {

    const { getCollapseProps, getToggleProps } = useCollapse();

    return (
        <div className="collapsible">
            <div className="text-white bg-black header px-3 py-2" {...getToggleProps()}>
                Advanced Search &#128269;
            </div>
            <div className="mx-3 py-0" {...getCollapseProps()}>
                <div className="bg-light content">
                    { /* idk how to pass the component so we use a naive if/else if/else block */ }
                    <div> 
                        {(() => {
                            if (localSearch.formType == 'song') {
                                return (<SongSearchForm />);
                            }
                            else if (localSearch.formType == 'artist') {
                                return (<ArtistSearchForm />);
                            }
                            else if (localSearch.formType == 'album') {
                                return (<AlbumSearchForm />);
                            }
                            else {
                                return (<div className="m-0 p-0 b-0"></div>);
                            }
                        })()}

                    </div>
                </div>
            </div>
        </div>
    );
}

export default LocalSearch;