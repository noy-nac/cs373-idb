import React from 'react';

class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            q: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <form className="d-flex flex-row justify-content-center">

                    <div className="col-lg-4 col-md-8 col-sm-10">
                        <label className="visually-hidden" for="queryInput">Query</label>
                        <div className="input-group">
                            <div className="input-group-text">&#128269;</div>
                            <input
                                type="text"
                                className="form-control"
                                id="queryInput"
                                name="q"
                                placeholder="Song, Artist, Album, Label, or Genre"
                                value={this.state.songText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    <button type="submit" class="btn btn-dark">Search</button>
                </form>
            </div>
        );
    }
}

export default SearchBox;