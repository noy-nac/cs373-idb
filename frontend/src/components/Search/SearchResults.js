import { useEffect } from "react";
import parse from "html-react-parser"

// Import react router for navigation
import { Link } from "react-router-dom";

// Table on Songs page
const SearchResults = (searchResults) => {

    // Import script to sort table
    useEffect(() => {
        const script = document.createElement("script")
        script.src = "https://www.kryogenix.org/code/browser/sorttable/sorttable.js"
        script.async = true
        document.body.appendChild(script)

        return () => {
            document.body.removeChild(script)
        }
    })

    return (
        <div>
            <div className="bg-black" style={{ height: "2vh" }}></div>
            <div className="shadow-4 rounded-1 overflow-hidden p-1">
                <table className="table align-middle mb-0 bg-white sortable table-hover" width={"100%"} data-search-highlight="true">
                    <thead className="bg-light">
                        <tr>
                            {searchResults.headers.map((title) => {
                                return (
                                    <th style={{ cursor: "pointer" }}>{title}</th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {searchResults.results.map((res, index) => {
                            return (
                                <tr className="item">

                                    {/* Relevance */} {/* Metadata for backend ordering, should not be displayed. (Davie)*/}
                                    {/* <td>{res["hit"]}</td> */}

                                    {/* Song: song_id, song_name, is_explicit */}
                                    <td>
                                        <Link to={"/song/" + res["song_id"]} className="list-group-item list-group-item-action">
                                            <div className="d-flex align-items-center">
                                                <h6 className="mb-1 mt-1 p-1">{parse(res['song_name'])}</h6>
                                                <p className="mb-1 mt-1 p-1 text-muted">{(res["is_explicit"]) ? "E" : ""}</p>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Artist: artist (artist_id, artist_name) */}
                                    <td>
                                        {res["artist"].map((artist) => {
                                            return (
                                                
                                                <Link to={"/artist/" + artist["artist_id"]} className="list-group-item list-group-item-action">
                                                    <div className="d-flex align-items-center">
                                                        <img src={artist["image_url"]} alt="" style={{ width: "45px", height: "45px" }} className="rounded-circle"></img>

                                                        <div className="ms-3">
                                                            <p className="mb-1 mt-1">{parse(artist["artist_name"])}</p>
                                                        </div>
                                                      </div>
                                                </Link>
                                            )
                                        })}
                                    </td>

                                    {/* Album: album_id, album_name, track_no, image_url */}
                                    <td>
                                        <Link to={"/album/" + res["album_id"]} className="list-group-item list-group-item-action">
                                            <div className="d-flex align-items-center">
                                                <img src={res["image_url"]} alt="" style={{ width: "45px", height: "45px" }} className="rounded-circle"></img>

                                                <div className="ms-3">
                                                    <p className="mb-1 mt-1">{parse(res["album_name"])}</p>
                                                    <p className="mb-1 mt-1 text-muted">{"Track " + res["track_no"]}</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Release Date: release_date */}
                                    <td>{parse(res["label"])}</td>

                                    {/* Duration: duration */}
                                    <td>
                                        {res["genre"].map((g) => {
                                            return (
                                                <div className="d-flex align-items-center">
                                                    <p className="mb-1 mt-1">{parse(g)}</p>
                                                </div>
                                            )
                                        })}
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default SearchResults;
