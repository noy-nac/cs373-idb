import axios from 'axios'
import { useState, useEffect } from 'react'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundSearch from '../Background/BackgroundSearch';
import SearchResults from './SearchResults';
import Pagination from '../Paginate/pagination';

// Search page
const GlobalSearch = (globalSearch) => {

    const [searchResults, setSearchResults] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage] = useState(10)

    // Get data from backend (about.json)
    useEffect(() => {
        axios.get(API_URL + '/search/' + window.location.search)
            .then(res => {
                setSearchResults(res.data["Search"])

                console.log(res.data["Search"])
            })
            .catch(err => console.log(err))
    }, [])

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = searchResults.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = pageNumber => setCurrentPage(pageNumber)

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`}
                pages={
                    [{ "Title": "Main Menu", "Path": "/" },
                    { "Title": "Songs", "Path": "/songs/" },
                    { "Title": "Albums", "Path": "/albums/" },
                    { "Title": "Artists", "Path": "/artists/" },
                    { "Title": "About", "Path": "/about/" },
                    { "Title": "Visualizations", "Path": "/visualizations/" },
                    { "Title": "Other Visualizations", "Path": "/othervisualizations/" }
                    ]
                }></NavBar>

            <BackgroundSearch title={"Global Search"} subtitle={"No Maidens?"} pic={`../static/images/musiclogo.png`}/>


            {/* <SearchResults results={searchResults} headers={["Hits", "Song", "Artist", "Album", "Label", "Genres"]}/> */}
            <SearchResults results={currentPosts} headers={["Song", "Artist", "Album", "Label", "Genres"]}/>
            <Pagination
                postsPerPage={postsPerPage}
                totalPosts={searchResults.length}
                paginate={paginate}
                currentPage={currentPage}
            />
        </div>
    );
}

export default GlobalSearch;
