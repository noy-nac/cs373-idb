import axios from 'axios'
import {useState, useEffect} from 'react'
import { API_URL } from '../Global'
import { GROUP_2_API_URL } from '../Global'

// Import componenets
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import GameCard from './Card/GameCard';
import GenreCard from './Card/GenreCard';
import CompanyCard from './Card/CompanyCard';

// Other visualizations page to display Game, Genre, and Company data
const OtherVisualizations = (otherVisual) => {

    const [gamesData, setGamesData] = useState([])
    const [genresData, setGenresData] = useState([])
    const [companiesData, setCompaniesData] = useState([])

    // TODO: Change calls to use their API
    
    // Get data from backend (game_data.json)
    useEffect(() => {
        axios.get(API_URL + '/games/')
            .then(res => {
                // Count number of games per platform
                const mapping = new Map();
                res.data["Games"].forEach((game) => {
                    game["platforms"].forEach((platform) => {
                        if (mapping.get(platform)) {
                            mapping.set(platform, mapping.get(platform) + 1)
                        } else {
                            mapping.set(platform, 1)
                        }
                    })
                })
                // Convert to format [{label:, value:}]
                let result = Array.from(mapping, ([label, value]) => ({label, value}))
                setGamesData(result)
            })
            .catch(err => console.log(err))
    }, [])

    // Get data from backend (genre_data.json)
    useEffect(() => {
        axios.get(API_URL + '/genres/')
            .then(res => {
                setGenresData(res.data["Genres"])
            })
            .catch(err => console.log(err))
    }, [])

    // Get data from backend (company_data.json)
    useEffect(() => {
        axios.get(API_URL + '/companies/')
            .then(res => {
                // Count the number of games per company location
                const mapping = new Map();
                res.data["Companies"].forEach((company) => {
                    if (mapping.get(company.location)) {
                        mapping.set(company.location, mapping.get(company.location) + company.num_games)
                    } else {
                        mapping.set(company.location, company.num_games)
                    }
                })
                // Convert to format [{label:, value:}]
                let result = Array.from(mapping, ([label, value]) => ({label, value}))
                setCompaniesData(result)
            }, [])
            .catch(err => console.log(err))
    })

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>
            <BackgroundGradient title={"Other Visualizations"} subtitle={"Analyze GameHub Data"} pic={`../static/images/gamelogo.png`}></BackgroundGradient>

            {/* Card for each visualization */}
            <div className="container-fluid mt-5 mb-5">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <GameCard data={gamesData}></GameCard>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <GenreCard data={genresData}></GenreCard>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <CompanyCard data={companiesData}></CompanyCard>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OtherVisualizations;
