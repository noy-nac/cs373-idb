// Import components
import PiePlot from "../Graph/PiePlot";

// Card for Artist visualization's title, description, and graph on Visualization page
const ArtistCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Artist</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Total Artists per Genre</h3>

                {/* If data not loaded show spinner  */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Plot */}
                        <PiePlot data={card.data.sort((artist_1, artist_2) => {
                                            return (artist_1.label).localeCompare(artist_2.label)
                                        })}
                            datakey="value" namekey="label" legendposition="bottom">
                        </PiePlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Groups artists by all of their genres.
                        Sums the total number of artists for each individual genre.
                        Displays each genre and its corresponding artist count in a pie chart.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default ArtistCard;
