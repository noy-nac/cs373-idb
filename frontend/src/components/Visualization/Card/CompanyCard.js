// Import components
import BarPlot from "../Graph/BarPlot";

// Card for Company visualization's title, description, and graph on Other Visualization page
const CompanyCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Company</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Location versus Total Games</h3>

                {/* If data not loaded show spinner */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Plot */}
                        <BarPlot data={card.data} xkey="label" xname="location" 
                            datakey="value" dataname="num_games">
                        </BarPlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Groups companies by their location.
                        Sums the total number of games for each company location.
                        Displays the trend between location and the total number of games produced.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default CompanyCard;
