// Import components
import PiePlot from "../Graph/PiePlot";

// Card for Album visualization's title, description, and graph on Visualization page
const AlbumCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Album</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Total Albums per Label</h3>

                {/* If data not loaded show spinner */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Plot */}
                        <PiePlot data={card.data.sort((album_1, album_2) => {
                                            return (album_1.label).localeCompare(album_2.label)
                                        })}
                            datakey="value" namekey="label" legendposition="bottom">
                        </PiePlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Groups albums by their label name.
                        Sums the total number of albums for each label.
                        Displays each label and its corresponding album count in a pie chart.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default AlbumCard;
