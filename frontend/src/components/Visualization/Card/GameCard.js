// Import components
import PiePlot from "../Graph/PiePlot";

// Card for Game visualization's title, description, and graph on Other Visualization page
const GameCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Game</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Total Games per Platform</h3>

                {/* If data not loaded show spinner  */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Plot */}
                        <PiePlot data={card.data.sort((platform_1, platform_2) => {
                                            return (platform_1.label).localeCompare(platform_2.label)
                                        })}
                            datakey="value" namekey="label" legendposition="top">
                        </PiePlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Groups games by all of their available platforms.
                        Sums the total number of games for each individual platform.
                        Displays each platform and its corresponding game count in a pie chart.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default GameCard;
