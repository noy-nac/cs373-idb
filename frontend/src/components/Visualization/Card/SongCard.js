// Import components
import ScatterPlot from '../Graph/ScatterPlot';

// Card for Song visualization's title, description, and graph on Visualization page
const SongCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Song</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Duration versus Popularity</h3>

                {/* If data not loaded show spinner */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Add key for total seconds */}
                        <div>
                            {card.data.forEach((song) => {
                                song.seconds = ((parseInt(song["duration"].split(":")[0]) * 60) + parseInt(song["duration"].split(":")[1]))
                            })}
                        </div>

                        {/* Sort songs by total seconds */}
                        {/* Plot duration versus popularity */}
                        <ScatterPlot   xkey="seconds"      xname="seconds"
                                        ykey="popularity"   yname="popularity"
                                        zkey="song_name"    zname="song_name"

                                        data={card.data.sort((song_1, song_2) => {
                                            return song_1.seconds - song_2.seconds
                                        })}
                                        name="song">
                        </ScatterPlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Displays the trend between song duration and popularity score. 
                        The song duration is the length of the song in seconds.
                        The song popularity score is a rank given by Spotify ranging from 0 to 100.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default SongCard;
