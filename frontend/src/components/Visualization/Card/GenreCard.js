// Import components
import PiePlot from "../Graph/PiePlot";

// Card for Genre visualization's title, description, and graph on Other Visualization page
const GenreCard = (card) => {
    return (
        <div className="card" style={{backgroundColor: "black"}}>
            <div className="card-body" style={{color: "white"}}>

                {/* Title */}
                <h2 className="card-title">Genre</h2>
                <h3 className="card-subtitle mb-2 text-secondary">Total Games per Genre</h3>

                {/* If data not loaded show spinner  */}
                {(card.data.length == 0)?
                    (<div className="d-flex justify-content-center p-5">
                        <div className="spinner-border text-light" role="status"></div>
                    </div>):
                    (<div>
                        {/* Plot */}
                        <PiePlot data={card.data.sort((genre_1, genre_2) => {
                                            return (genre_1.name).localeCompare(genre_2.name)
                                        })}
                            datakey="num_games" namekey="name" legendposition="top">
                        </PiePlot>
                    </div>)
                }
                {/* Description */}
                <div className="container d-flex justify-content-center text-center p-5">
                    <h5>
                        Displays each genre and its corresponding game count in a pie chart.
                    </h5>
                </div>
            </div>
        </div>
    )
}

export default GenreCard;
