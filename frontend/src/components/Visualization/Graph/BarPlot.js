import { CartesianGrid, XAxis, YAxis, Tooltip, Legend, BarChart, Bar } from "recharts";

// Bar graph for Company visualization
const BarPlot = (bar) => {
    return (
        <div className="d-flex justify-content-center">
            <BarChart width={1200} height={500} data={bar.data} margin={{top:5, right:30, left:20, bottom:5}}>
                <CartesianGrid strokeDasharray="3 3"></CartesianGrid>
                <XAxis dataKey={bar.xkey} name={bar.xname}></XAxis>
                <YAxis dataKey={bar.datakey} name={bar.dataname}></YAxis>
                <Tooltip cursor={{strokeDasharray: '3 3'}}></Tooltip>
                <Legend></Legend>
                <Bar type="monotone" dataKey={bar.datakey} name={bar.dataname} fill="#8884d8"></Bar>
            </BarChart>
        </div>
    )
}

export default BarPlot;
