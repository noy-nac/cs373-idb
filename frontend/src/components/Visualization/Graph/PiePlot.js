import { Tooltip, Legend, PieChart, Pie, Cell } from 'recharts';

// Pie chart for Album, Artist, Game and Genre visualizations
const PiePlot = (pie) => {
    
    const colors = ["darkturquoise","antiquewhite","aqua","aquamarine","gold","hotpink",
        "bisque","indigo","blueviolet","brown","burlywood","cadetblue","chartreuse","coral",
        "mediumorchid","navy","darkcyan"];
    
    return (
        <div className="d-flex justify-content-center">
            <PieChart width={1200} height={900} margin={{top:5, right:30, bottom:20, left:5}}>
                <Tooltip cursor={{strokeDasharray: '3 3'}}></Tooltip>
                <Legend layout="horizontal" verticalAlign={pie.legendposition} align="center"></Legend>
                <Pie data={pie.data} dataKey={pie.datakey} nameKey={pie.namekey} legendType="line"
                    cx="50%" cy="50%" outerRadius={300} fill="#8884d8" label>
                    {pie.data.map((entry, index) => (
                        // Mod trick for circular array access: (i % n + n) % n
                        <Cell key={`cell-${index}`} fill={colors[(index % colors.length + colors.length) % colors.length]}></Cell>
                    ))}
                </Pie>
            </PieChart>
        </div>
    )
}

export default PiePlot;
