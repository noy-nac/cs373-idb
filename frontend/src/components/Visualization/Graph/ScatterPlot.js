import { CartesianGrid, XAxis, YAxis, ZAxis, Tooltip, Legend, ScatterChart, Scatter } from 'recharts';

// Scatter plot for Song visualization
const ScatterPlot = (scatter) => {
    return (
        <div className="d-flex justify-content-center">
            <ScatterChart width={1200} height={500} margin={{top:5, right:30, bottom:20, left:5}}>
                <CartesianGrid strokeDasharray="3 3"></CartesianGrid>
                <XAxis dataKey={scatter.xkey} name={scatter.xname}></XAxis>
                <YAxis dataKey={scatter.ykey} name={scatter.yname}></YAxis>
                <ZAxis dataKey={scatter.zkey} name={scatter.zname}></ZAxis>
                <Tooltip cursor={{strokeDasharray: '3 3'}}></Tooltip>
                <Legend></Legend>
                <Scatter name={scatter.name} data={scatter.data} fill="#8884d8"></Scatter>
            </ScatterChart>
        </div>
    )
}

export default ScatterPlot;
