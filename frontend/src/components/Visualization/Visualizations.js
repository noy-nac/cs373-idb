import axios from 'axios'
import {useState, useEffect} from 'react'
import { API_URL } from '../Global'

// Import componenets
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import SongCard from './Card/SongCard';
import AlbumCard from './Card/AlbumCard';
import ArtistCard from './Card/ArtistCard';

// Visualizations page to display Song, Album, and Artist data
const Visualizations = (visual) => {

    const [songsData, setSongsData] = useState([])
    const [albumsData, setAlbumsData] = useState([])
    const [artistsData, setArtistsData] = useState([])

    // Get data from backend (song_data.json)
    useEffect(() => {
        axios.get(API_URL + '/songs/')
            .then(res => {
                setSongsData(res.data["Songs"])
            })
            .catch(err => console.log(err))
    }, [])

    // Get data from backend (album_data.json)
    useEffect(() => {
        axios.get(API_URL + '/albums/')
            .then(res => {
                // Count number of albums per label
                const mapping = new Map();
                res.data["Albums"].forEach((album) => {
                    if (mapping.get(album.label)) {
                        mapping.set(album.label, mapping.get(album.label) + 1)
                    } else {
                        mapping.set(album.label, 1)
                    }
                })
                // Convert to format [{label:, value:}]
                let result = Array.from(mapping, ([label, value]) => ({label, value}))
                setAlbumsData(result)
            })
            .catch(err => console.log(err))
    }, [])

    // Get data from backend (artist_data.json)
    useEffect(() => {
        axios.get(API_URL + '/artists/')
            .then(res => {
                // Count number of artists per genre
                const mapping = new Map();
                res.data["Artists"].forEach((artist) => {
                    artist["genre"].forEach((genre) => {
                        if (mapping.get(genre)) {
                            mapping.set(genre, mapping.get(genre) + 1)
                        } else {
                            mapping.set(genre, 1)
                        }
                    })
                })
                // Convert to format [{label:, value:}]
                let result = Array.from(mapping, ([label, value]) => ({label, value}))
                setArtistsData(result)
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>
            <BackgroundGradient title={"Visualizations"} subtitle={"Analyze Our Data"} pic={`../static/images/musiclogo.png`}></BackgroundGradient>

            {/* Card for each visualization */}
            <div className="container-fluid mt-5 mb-5">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <SongCard data={songsData}></SongCard>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <AlbumCard data={albumsData}></AlbumCard>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 p-2">
                        <ArtistCard data={artistsData}></ArtistCard>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Visualizations;
