import axios from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { API_URL } from '../Global'

// Page that runs unittests
const Output = (output) => {

    const [data, setData] = useState([])

    const { filename } = useParams();

    // Get filename from backend
    useEffect(() => {
        axios.get(API_URL + '/output/' + filename)
            .then(res => {
                setData(res.data["Output"])
            })
            .catch(err => console.log(err))
    })

    return (
        <div className="container-fluid mt-3 mb-3">
            <div className="row mb-3"><h6>Output of {filename}.py</h6></div>
            <div className="row">
                {data.map((line) => {
                    return (
                        <p>{line}</p>
                    )
                })}
            </div>
        </div>
    );
}

export default Output;
