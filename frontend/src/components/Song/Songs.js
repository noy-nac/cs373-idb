import axios from 'axios'
import { useState, useEffect } from 'react'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundCarousel from '../Background/BackgroundCarousel';
import LocalSearch from '../Search/LocalSearch';
import SongsTable from './SongsTable';
import Pagination from '../Paginate/pagination';

// Songs page
const Songs = (songs) => {

    const [songsData, setSongsData] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage] = useState(10)

    // Get data from backend (song_data.json)
    useEffect(() => {
        axios.get(API_URL + '/songs/' + window.location.search)
            .then(res => {
                setSongsData(res.data["Songs"])
            })
            .catch(err => console.log(err))
    }, [])

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = songsData.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = pageNumber => setCurrentPage(pageNumber)
        
    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>
            <BackgroundCarousel title={"Find Songs"} 
                pics={[
                    "https://www.adobe.com/content/dam/cc/us/en/creativecloud/photography/discover/concert-photography/thumbnail.jpeg",
                    "https://cdn.mos.cms.futurecdn.net/rtKz3ipbmux3aR2gnNqKCC.jpg"
                ]}>
            </BackgroundCarousel>
            <LocalSearch formType="song"/>
            <SongsTable songs={currentPosts} headers={["Rank", "Song", "Artist", "Album", "Release Date", "Duration"]}></SongsTable>
            <Pagination
                postsPerPage={postsPerPage}
                totalPosts={songsData.length}
                paginate={paginate}
                currentPage={currentPage}
            />
        </div>
    );
}

export default Songs;
