import React from 'react';

class SongSearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            songText: "",
            albumText: "",
            artistText: "",
            releasedFrom: "",
            releasedTo: "",
            durationMin: "",
            durationMax: "",
            hideExplicit:""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <form>
                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Search by Attribute</legend>

                    {/* SONG TITLE */}
                    <div className="col-lg-2 col-md-3 col-sm-4">
                        <label className="visually-hidden" for="songTextInput">Song Title</label>
                        <div className="input-group">
                            <div className="input-group-text">Title</div>
                            <input
                                type="text"
                                className="form-control"
                                id="songTextInput"
                                name="songText"
                                placeholder="Song Title"
                                value={this.state.songText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* ARTIST NAME */}
                    <div className="col-lg-2 col-md-3 col-sm-4">
                        <label className="visually-hidden" for="artistTextInput">Artist Name</label>
                        <div className="input-group">
                            <div className="input-group-text">By</div>
                            <input
                                type="text"
                                className="form-control"
                                id="artistTextInput"
                                name="artistText"
                                placeholder="Artist Name"
                                value={this.state.artistText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* ALBUM TITLE */}
                    <div className="col-lg-2 col-md-3 col-sm-4">
                        <label className="visually-hidden" for="albumTextInput">Album Title</label>
                        <div className="input-group">
                            <div className="input-group-text">In</div>
                            <input
                                className="form-control"
                                type="text"
                                id="albumTextInput"
                                name="albumText"
                                placeholder="Album Title"
                                value={this.state.albumText}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                    
                </fieldset>
                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Released Between</legend>

                    {/* Released From */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="releasedFromInput">Released From</label>
                        <div className="input-group">
                            <div className="input-group-text">From</div>
                            <input
                                type="date"
                                className="form-control"
                                id="releasedFromInput"
                                name="releasedFrom"
                                value={this.state.releasedFrom}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>

                    {/* Released To */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="releasedToInput">Released To</label>
                        <div className="input-group">
                            <div className="input-group-text">To</div>
                            <input
                                type="date"
                                className="form-control"
                                id="releasedToInput"
                                name="releasedTo"
                                value={this.state.releasedTo}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                </fieldset>

                <fieldset className="row gx-3 gy-0 mb-2 align-items-center">
                    <legend className="my-0">Running Time</legend>


                    {/* Duration Min */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="durationMinInput">Min Duration</label>
                        <div className="input-group">
                            <div className="input-group-text">Min</div>
                            <input
                                type="text"
                                className="form-control"
                                id="durationMinInput"
                                name="durationMin"
                                value={this.state.durationMin}
                                onChange={this.handleInputChange}
                                pattern="[0-9]{0,1,2}:[0-9]{2}:[0-9]{2}"
                                placeholder="hh:mm:ss" 
                                title="Format durations using hh:mm:ss." 
                            />
                        </div>
                    </div>

                    {/* Duration Max */}
                    <div className="col-lg-3 col-md-4 col-sm-6">
                        <label className="visually-hidden" for="durationMaxInput">Max Duration</label>
                        <div className="input-group">
                            <div className="input-group-text">Max</div>
                            <input
                                type="text"
                                className="form-control"
                                id="durationMaxInput"
                                name="durationMax"
                                value={this.state.durationMax}
                                onChange={this.handleInputChange}
                                pattern="[0-9]{0,1,2}:[0-9]{2}:[0-9]{2}"
                                placeholder="hh:mm:ss"
                                title="Format durations using hh:mm:ss." 
                            />
                        </div>
                    </div>
                </fieldset>

                <input
                    type="checkbox"
                    className="form-check-input"
                    id="hideExplicitCheck"
                    name="hideExplicit"
                    value={this.state.hideExplicit}
                    onChange={this.handleInputChange}
                />
                <label className="form-check-label mx-2" for="hideExplicitCheck">Hide Explicit</label>

                <div class="col-auto ">
                    <button type="submit" class="btn btn-dark my-2">Search</button>
                </div>
            </form>
        );
    }
}

export default SongSearchForm;