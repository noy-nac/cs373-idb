// Import react router for navigation
import { Link } from "react-router-dom";

// Cards that display a particular song's details on the Song page
const SongCards = (songCards) => {
    return (
        <div>
            <div className="bg-white" style={{height: "5vh"}}></div>
            <div className="d-flex align-items-center">
                <div className="container-fluid">
                    <div className="row">

                        {/* Artists */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>Artists</strong></h5>
                                    <div className="container-fluid">
                                        <div className="row">

                                            {/* MUST have ? to avoid map error */}
                                            {(songCards.songData["artist"])?.map((artist) => {
                                                return (
                                                    <div className="col p-2">
                                                        <Link to={"/artist/" + artist["artist_id"]} className="btn btn-outline-light">{artist["artist_name"]}</Link>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Album and track number */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{songCards.songData["album_name"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Album</h6>
                                    <h6 className="card-subtitle mb-2 text-secondary">{"Track No. " + songCards.songData["track_no"]}</h6>
                                    <Link to={"/album/" + songCards.songData["album_id"]} className="btn btn-outline-light">View Album</Link>
                                </div>
                            </div>
                        </div>

                        {/* Release date */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{songCards.songData["release_date"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Release Date</h6>
                                </div>
                            </div>
                        </div>

                        {/* Duration */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{songCards.songData["duration"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Duration</h6>
                                </div>
                            </div>
                        </div>

                        {/* Popularity */}
                        <div className="col my-1" style={{height: "20vh"}}>
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{songCards.songData["popularity"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Rank</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SongCards;
