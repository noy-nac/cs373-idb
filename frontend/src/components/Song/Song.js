import axios from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import SongCards from './SongCards';

// Song details page
const Song = (song) => {

    const [songData, setSongData] = useState({})

    const { id } = useParams();

    // Get data from backend
    useEffect(() => {
        axios.get(API_URL + '/song/' + id)
            .then(res => {
                setSongData(res.data)
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>

            {/* Set background to song name, explicitly, and album image */}
            <BackgroundGradient title={songData["song_name"]} 
                subtitle={(songData["is_explicit"])? "Explicit": "Clean"} 
                pic={songData["image_url"]}>
            </BackgroundGradient>
            
            {/* Set cards to artists, album, release date, duration, and popularity */}
            <SongCards songData={songData}></SongCards>
        </div>
    );
}

export default Song;
