import { useEffect } from "react";

// Import react router for navigation
import { Link } from "react-router-dom";

// Table on Songs page
const SongsTable = (songsTable) => {

    // Import script to sort table
    useEffect(() => {
        const script = document.createElement("script")
        script.src = "https://www.kryogenix.org/code/browser/sorttable/sorttable.js"
        script.async = true
        document.body.appendChild(script)

        return () => {
            document.body.removeChild(script)
        }
    })

    return (
        <div>
            <div className="bg-black" style={{height: "2vh"}}></div>
            <div className="shadow-4 rounded-1 overflow-hidden p-1">
                <table className="table align-middle mb-0 bg-white sortable table-hover" width={"100%"}>
                    <thead className="bg-light">
                        <tr>
                            {songsTable.headers.map((title) => {
                                return (
                                    <th style={{cursor: "pointer"}}>{title}</th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {songsTable.songs.map((song, index) => {
                            return (
                                <tr className="item">

                                    {/* Rank: popularity */}
                                    <td>{song["popularity"]}</td>

                                    {/* Song: song_id, song_name, is_explicit */}
                                    <td>
                                        <Link to={"/song/" + song["song_id"]} className="list-group-item list-group-item-action">
                                            <div className = "d-flex align-items-center">
                                                <h6 className="mb-1 mt-1 p-1">{song["song_name"]}</h6>
                                                <p className="mb-1 mt-1 p-1 text-muted">{(song["is_explicit"])? "E": ""}</p>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Artist: artist (artist_id, artist_name) */}
                                    <td>
                                        {song["artist"].map((artist) => {
                                            return (
                                                <Link to={"/artist/" + artist["artist_id"]} className="list-group-item list-group-item-action">
                                                    <div className = "d-flex align-items-center">
                                                        <p className="mb-1 mt-1">{artist["artist_name"]}</p>
                                                    </div>
                                                </Link>
                                            )
                                        })}
                                    </td>

                                    {/* Album: album_id, album_name, track_no, image_url */}
                                    <td>
                                        <Link to={"/album/" + song["album_id"]} className="list-group-item list-group-item-action">
                                            <div className = "d-flex align-items-center">
                                                <img src={song["image_url"]} alt="" style={{width: "45px", height: "45px"}} className="rounded-circle"></img>

                                                <div className="ms-3">
                                                    <p className="mb-1 mt-1">{song["album_name"]}</p>
                                                    <p className="mb-1 mt-1 text-muted">{"Track " + song["track_no"]}</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Release Date: release_date */}
                                    <td>{song["release_date"]}</td>

                                    {/* Duration: duration */}
                                    <td>{song["duration"]}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default SongsTable;
