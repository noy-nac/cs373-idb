import { useEffect } from "react";

// Import react router for navigation
import { Link } from "react-router-dom";

// Table on Albums page
const AlbumsTable = (albumsTable) => {

    // Import script to sort table
    useEffect(() => {
        const script = document.createElement("script")
        script.src = "https://www.kryogenix.org/code/browser/sorttable/sorttable.js"
        script.async = true
        document.body.appendChild(script)

        return () => {
            document.body.removeChild(script)
        }
    })

    return (
        <div>
            <div className="bg-black" style={{height: "2vh"}}></div>
            <div className="shadow-4 rounded-1 overflow-hidden p-1">
                <table className="table align-middle mb-0 bg-white sortable table-hover" width={"100%"}>
                    <thead className="bg-light">
                        <tr>
                            {albumsTable.headers.map((title) => {
                                return (
                                    <th style={{cursor: "pointer"}}>{title}</th>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {albumsTable.albums.map((album, index) => {
                            return (
                                <tr className="item">

                                    {/* Album: album_id, album_name, is_explicit, image_url */}
                                    <td>
                                        <Link to={"/album/" + album["album_id"]} className="list-group-item list-group-item-action">
                                            <div class = "d-flex align-items-center">
                                                <img src={album["image_url"]} alt="" style={{width: "45px", height: "45px"}} className="rounded-circle"></img>

                                                <div className="ms-3">
                                                    <h6 className="mb-1 mt-1">{album["album_name"]}</h6>
                                                    <p className="mb-1 mt-1 text-muted">{(album["is_explicit"])? "E": ""}</p>
                                                </div>
                                            </div>
                                        </Link>
                                    </td>

                                    {/* Artist: artist (artist_id, artist_name) */}
                                    <td>
                                        {album["artist"].map((artist) => {
                                            return (
                                                <Link to={"/artist/" + artist["artist_id"]} className="list-group-item list-group-item-action">
                                                    <div class = "d-flex align-items-center">
                                                        <p className="mb-1 mt-1">{artist["artist_name"]}</p>
                                                    </div>
                                                </Link>
                                            )
                                        })}
                                    </td>

                                    {/* Songs: song_count */}
                                    <td>{album["song_count"]}</td>

                                    {/* Release Date: release_date */}
                                    <td>{album["release_date"]}</td>

                                    {/* Label: label */}
                                    <td>{album["label"]}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default AlbumsTable;
