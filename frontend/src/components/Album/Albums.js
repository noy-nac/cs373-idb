import axios from 'axios'
import {useState, useEffect} from 'react'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundCarousel from '../Background/BackgroundCarousel';
import LocalSearch from '../Search/LocalSearch';
import AlbumsTable from './AlbumsTable';
import Pagination from '../Paginate/pagination';

// Albums page
const Albums = (albums) => {

    const [albumsData, setAlbumsData] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage] = useState(10)

    // Get data from backend (album_data.json)
    useEffect(() => {
        axios.get(API_URL + '/albums/' + window.location.search)
            .then(res => {
                setAlbumsData(res.data["Albums"])
            })
            .catch(err => console.log(err))
    }, [])

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = albumsData.slice(indexOfFirstPost, indexOfLastPost);

    const paginate = pageNumber => setCurrentPage(pageNumber)

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
                pages={
                    [{"Title": "Main Menu", "Path": "/"},
                    {"Title": "Songs", "Path": "/songs/"},
                    {"Title": "Albums", "Path": "/albums/"},
                    {"Title": "Artists", "Path": "/artists/"},
                    {"Title": "About", "Path": "/about/"},
                    {"Title": "Visualizations", "Path": "/visualizations/"},
                    {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                    ]
                }>
            </NavBar>
            <BackgroundCarousel title={"Explore Albums"} 
                pics={[
                    "https://mcdn.wallpapersafari.com/medium/77/35/RKCb01.jpg",
                    "https://mcdn.wallpapersafari.com/medium/66/29/NXbtVu.jpg"
                ]}>
            </BackgroundCarousel>
            <LocalSearch formType="album" />
            <AlbumsTable albums={currentPosts} headers={["Album", "Artist", "Songs", "Release Date", "Label"]}></AlbumsTable>
            <Pagination
                postsPerPage={postsPerPage}
                totalPosts={albumsData.length}
                paginate={paginate}
                currentPage={currentPage}
            />
        </div>
    );
}

export default Albums;
