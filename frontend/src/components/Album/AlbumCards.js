// Import react router for navigation
import { Link } from "react-router-dom";

// Cards that display a particular album's details on the Album page
const AlbumCards = (albumCards) => {
    return (
        <div>
            <div className="bg-white" style={{height: "5vh"}}></div>
            <div className="d-flex align-items-center">
                <div className="container-fluid">
                    <div className="row">

                        {/* Artists */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>Artists</strong></h5>
                                    <div className="container-fluid">
                                        <div className="row">

                                            {/* MUST have ? to avoid map error */}
                                            {(albumCards.albumData["artist"])?.map((artist) => {
                                                return (
                                                    <div className="col p-2">
                                                        <Link to={"/artist/" + artist["artist_id"]} className="btn btn-outline-light">{artist["artist_name"]}</Link>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Number of songs */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{albumCards.albumData["song_count"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Songs</h6>
                                </div>
                            </div>
                        </div>

                        {/* Release date */}
                        <div className="col my-1">
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{albumCards.albumData["release_date"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Release Date</h6>
                                </div>
                            </div>
                        </div>

                        {/* Label */}
                        <div className="col my-1" style={{height: "20vh"}}>
                            <div className="card h-100" style={{backgroundColor: "black"}}>
                                <div className="card-body" style={{color: "white"}}>
                                    <h5 className="card-title"><strong>{albumCards.albumData["label"]}</strong></h5>
                                    <h6 className="card-subtitle mb-2 text-secondary">Label</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AlbumCards;
