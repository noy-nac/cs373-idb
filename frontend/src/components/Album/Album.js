import axios from 'axios'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { API_URL } from '../Global'

// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundGradient from '../Background/BackgroundGradient';
import AlbumCards from './AlbumCards';

// Album details page
const Album = (album) => {

    const { id } = useParams();

    const [albumData, setAlbumData] = useState({})

    // Get data from backend
    useEffect(() => {
        axios.get(API_URL + '/album/' + id)
            .then(res => {
                setAlbumData(res.data)
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div>
            <NavBar title="HTMelodies" pic={`../static/images/musiclogo.png`} 
            pages={
                [{"Title": "Main Menu", "Path": "/"},
                 {"Title": "Songs", "Path": "/songs/"},
                 {"Title": "Albums", "Path": "/albums/"},
                 {"Title": "Artists", "Path": "/artists/"},
                 {"Title": "About", "Path": "/about/"},
                 {"Title": "Visualizations", "Path": "/visualizations/"},
                 {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]
            }></NavBar>

            {/* Set background to album name, explicitly, and album image */}
            <BackgroundGradient title={albumData["album_name"]} 
                subtitle={(albumData["is_explicit"])? "Explicit": "Clean"} 
                pic={albumData["image_url"]}>
            </BackgroundGradient>

            {/* Set cards to artists, release date, and label */}
            <AlbumCards albumData={albumData}></AlbumCards>
        </div>
    );
}

export default Album;
