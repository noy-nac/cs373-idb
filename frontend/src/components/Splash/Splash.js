// Import components
import NavBar from '../NavBar/NavBar';
import BackgroundSplash from '../Background/BackgroundSplash';
import SplashIcons from './SplashIcons';

// Splash page
const Splash = (splash) => {
    return (
        <div>
            <NavBar title={"HTMelodies"} pic={`../static/images/musiclogo.png`} 
                pages={[
                    {"Title": "Main Menu", "Path": "/"},
                    {"Title": "Songs", "Path": "/songs/"},
                    {"Title": "Albums", "Path": "/albums/"},
                    {"Title": "Artists", "Path": "/artists/"},
                    {"Title": "About", "Path": "/about/"},
                    {"Title": "Visualizations", "Path": "/visualizations/"},
                    {"Title": "Other Visualizations", "Path": "/othervisualizations/"}
                ]}>
            </NavBar>
            <BackgroundSplash title={"HTMelodies"} subtitle={"Musical Fidelity"} pic={`../static/images/chevron.png`}></BackgroundSplash>
            <SplashIcons titleColor={"rgba(100, 11, 229, 0.863)"}
                titles={[
                    {"Title": "Songs", "Path": "/songs/"}, 
                    {"Title": "Albums", "Path": "/albums/"}, 
                    {"Title": "Artists", "Path": "/artists/"}
                ]}
                pics={[
                    {"Link": "https://i.scdn.co/image/ab671c3d0000f430678067184805befd38aa0f57", "Path": "/songs/"},
                    {"Link": "https://i.scdn.co/image/ab671c3d0000f430a6f822749d5c25c32ee31d66", "Path": "/albums/"},
                    {"Link": "https://i.scdn.co/image/ab671c3d0000f430eb43023b5332389f5094530d", "Path": "/artists/"}
                ]}>
            </SplashIcons>
        </div>
    );
}

export default Splash;
