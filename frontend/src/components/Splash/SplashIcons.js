// Import react router for navigation
import { Link } from "react-router-dom";

// Icons on Splash page for Songs, Albums, and Artists
const SplashIcons = (splashIcons) => {
    return (
        <div>
            <div className="container-fluid align-items-center">

                {/* Icon images and links */}
                <div className="row">
                    {splashIcons.pics.map((pic) => {
                        return (
                            <div className="col d-flex justify-content-center">
                                <Link to={pic["Path"]}>
                                    <img src={pic["Link"]} className="img-fluid" alt=""></img>
                                </Link>
                            </div>
                        )
                    })}
                </div>

                {/* Icon titles */}
                <div className="row mt-3">
                    {splashIcons.titles.map((title) => {
                        return (
                            <div className="col d-flex justify-content-center">
                                <Link to={title["Path"]} className="h2 text-decoration-none" style={{color: splashIcons.titleColor}}>
                                    <strong>{title["Title"]}</strong>
                                </Link>
                            </div>
                        )
                    })}
                </div>
            </div>
            <div className="bg-white" style={{height: "24vh"}}></div>
        </div>
    );
}

export default SplashIcons;
