import './BackgroundSplash.css';

// Sets background for Splash page
const BackgroundSplash = (bgSplash) => {
    return (
        <div>
            <div id="backgroundSplash" className="bg-image p-3" 
                style={{
                    backgroundImage: `url(${`../static/images/splashgradient.jpg`})`,
                    height: "87vh",
                    backgroundSize: "cover",
                    backgroundPosition: "center"
                }}>
                {/* Typing animation */}
                <div className="h-100 d-flex justify-content-center align-items-center">
                    <div className="typing">
                        <h1 className="text-white display-3">{bgSplash.title}</h1>
                    </div>
                </div>
                {/* Toggle to middle of screen */}
                <div className="position-relative top-0 start-50 translate-middle">
                    <div className="position-absolute bottom-0 start-50 translate-middle">
                        <a href="#icons">
                            <img
                                data-mdb-toggle="animation"
                                data-mdb-animation-start="onHover"
                                data-mdb-animation-reset="true"
                                data-mdb-animation="slide-right"
                                src={bgSplash.pic}
                                className="img-fluid"
                                width="50"
                                height="50"
                                style={{opacity: "0.5"}}
                                alt=""
                            />
                        </a>
                    </div>
                </div>
            </div>
            {/* Website slogan and target for toggle */}
            <div className="bg-black" style={{height: "5vh"}}></div>
            <div className="bg-white" style={{height: "44vh"}} id="icons">
                <div className="h-100 d-flex justify-content-center align-items-center">
                    <div>
                        <h1 className="text-black display-3">{bgSplash.subtitle}</h1>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BackgroundSplash;
