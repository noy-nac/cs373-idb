import './BackgroundSplash.css';

import BackgroundText from './BackgroundText'
import SearchBox from '../Search/SearchBox'

// Sets background for Searh page
const BackgroundSearch = (bgSearch) => {
    return (
        <div>
            <div id="backgroundSearch" className="bg-image d-flex align-items-center"
                style={{
                    backgroundImage: `url(${`../static/images/splashgradient.jpg`})`,
                    height: "55vh",
                    backgroundSize: "cover",
                    backgroundPosition: "center"
                }}>
                <div className="col">
                    <div className="p-5">
                        <h1 className="text-white display-1"><strong>{bgSearch.title}</strong></h1>
                    </div>
                    <SearchBox />
                </div>
            </div>
            <div className="bg-black" style={{ height: "2vh" }}></div>
        </div>
    );
}

export default BackgroundSearch;
