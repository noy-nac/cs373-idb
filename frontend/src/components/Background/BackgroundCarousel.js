// Sets background to carousel
const BackgroundCarousel = (bgCarousel) => {
    return (
        <div id="backgroundCarousel" className="carousel slide carousel-fade" data-bs-ride="carousel">
            {/* Carousel indicators */}
            <ol className="carousel-indicators">
                <li data-bs-target="#backgroundCarousel" data-bs-slide-to="0" className="active"></li>
                {(bgCarousel.pics).map((pic, index) => {
                    return (
                        <li data-bs-target="#backgroundCarousel" data-bs-slide-to={"" + (index + 1)}></li>
                    )
                })}
            </ol>
            {/* Carousel items */}
            <div className="carousel-inner">
                <div className="carousel-item active" data-bs-id="0">
                    <div className="bg-image"
                        style={{
                            backgroundImage: `url(${`../static/images/splashgradient.jpg`})`, 
                            height: "50vh"
                        }}>
                        <div className="h-100 d-flex align-items-center p-5">
                            <h1 className="text-white display-1"><strong>{bgCarousel.title}</strong></h1>
                        </div>
                    </div>
                </div>
                {(bgCarousel.pics).map((pic, index) => {
                    return (
                        <div className="carousel-item" data-bs-id={"" + (index + 1)}>
                            <div className="bg-image"
                                style={{
                                    backgroundImage: `url(${pic})`,
                                    height: "50vh",
                                    backgroundSize: "contain"
                                }}>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default BackgroundCarousel;
