import BackgroundText from './BackgroundText';

// Sets background to gradient
// Gives title, subtitle, and picture to BackgroundText
const BackgroundGradient = (bgGradient) => {
    return (
        <div id="backgroundGradient" className="bg-image d-flex align-items-center" 
            style={{
                backgroundImage: `url(${`../static/images/splashgradient.jpg`})`,
                height: "50vh",
                backgroundSize: "cover",
                backgroundPosition: "center"
            }}>
            <div className="container-fluid">
                <BackgroundText title={bgGradient.title} subtitle={bgGradient.subtitle} pic={bgGradient.pic}></BackgroundText>
            </div>
        </div>
    );
}

export default BackgroundGradient;
