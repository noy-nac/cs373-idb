// Given title, subtitle, and picture from BackgroundGradient
const BackgroundText = (bgText) => {
    return (
        <div className="row">
            <div className="col ms-5 d-flex align-items-center">
                <div>
                    <div className="row">
                        <h1 className="text-white display-2"><strong>{bgText.title}</strong></h1>
                    </div>
                    <div className="row">
                        <h2 className="text-light display-6">{bgText.subtitle}</h2>
                    </div>
                </div>
            </div>
            <div className="col d-flex align-items-center me-5">
                <img src={bgText.pic} alt="?" width="250" height="250" className="rounded-circle ms-auto me-5"></img>
            </div>
        </div>
    );
}

export default BackgroundText;
