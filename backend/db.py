import json
from models import app, db, Artist, Album, Song, Genre

# Making JSON keys constants at the top so we can match up names later on
JSON_ARTIST = "data/artist_data.json"
JSON_ALBUM = "data/album_data.json"
JSON_SONG = "data/song_data.json"
JSON_TEST_ARTIST = "data/artist_data.json"
JSON_TEST_ALBUM = "data/album_data.json"
JSON_TEST_SONG = "data/song_data.json"

# these keys are inconsistent between test and prod data....
# currently the keys work for PROD DATA ONLY

# used to specify the id of a parent artist(s), database defn assumes the possibility for mult atists
ARTIST_KEY = "artist"
ALBUM_KEY = "album"       # used to specify the id of a parent album
ARTIST_ID_KEY = "artist_id"
ALBUM_ID_KEY = "album_id"
SONG_ID_KEY = "song_id"
ARTIST_NAME_KEY = "artist_name"
ALBUM_NAME_KEY = "album_name"
SONG_NAME_KEY = "song_name"
ALBUM_COUNT_KEY = "album_count"
POPULARITY_KEY = "popularity"
SONG_COUNT_KEY = "song_count"      # calculated for artists
IS_SINGLE_KEY = "is_single"
RELEASE_DATE_KEY = "release_date"    # calculated (inherit) for songs
DURATION_KEY = "duration"        # calculated for albums
IS_EXPLICIT_KEY = "is_explicit"
TRACK_NO_KEY = "track_no"
GENRE_KEY = "genre"
FOLLOWER_COUNT_KEY = "follower_count"
ALBUM_RELEASE_DATE_KEY = "release_date"
ALBUM_LABEL_KEY = "label"
ALBUM_TRACK_COUNT_KEY = "track_count"
ALBUM_EXPLICIT_KEY = "is_explicit"
SONG_IMAGE_URL_KEY = "image_url"
ALBUM_IMAGE_URL_KEY = "image_url"
ARTIST_IMAGE_URL_KEY = "image_url"


# Reset the database
def reset_db():
    print('reset db called')
    db.session.remove()
    db.drop_all()
    db.create_all()


# Populate database with songs, albums, and artists (ORDER MATTERS)
def populate_db(isTest=False):
    print("populate db called")
    populate_songs(populatedArtist=False, populatedAlbum=False, isTest=isTest)
    populate_albums(populatedArtist=False, populatedSong=True, isTest=isTest)
    populate_artists(populatedAlbum=True, populatedSongs=True, isTest=isTest)


# Populate the database with artists
def populate_artists(populatedAlbum, populatedSongs, isTest=False):
    json_file = JSON_TEST_ARTIST if isTest else JSON_ARTIST
    with open(json_file) as jsn:
        artist_data = json.load(jsn)

        if isTest:
            print('\tartist_data:\t', artist_data, '\n')

        for artist in artist_data:
            if isTest:
                print('\tartist:\t', artist, '\n')

            # col names come from db table defn and should not be changed!
            db_row = dict({
                'artist_id': artist[ARTIST_ID_KEY],
                'artist_name': artist[ARTIST_NAME_KEY],
                'follower_count': artist[FOLLOWER_COUNT_KEY],
                'genre': [],  # fill later
                'popularity': int(artist[POPULARITY_KEY]),
                'image_url': artist[ARTIST_IMAGE_URL_KEY]
            })

            # link up any existing artists or add a temp placeholder entry
            # need to add the object for some reason
            genre_list = artist[GENRE_KEY]
            for g_id in genre_list:
                genre_query = db.session.query(Genre).filter(
                    Genre.genre_id == g_id).all()

                if isTest:
                    print('\tgenre_query:\t', genre_query)

                # genre_id is the PK so if we have multiple entries, we're in trouble
                assert len(genre_query) <= 1

                if len(genre_query) == 1:
                    genre_obj = genre_query[0]
                    db_row['genre'].append(genre_obj)
                else:
                    genre_obj = Genre(genre_id=g_id)
                    db_row['genre'].append(genre_obj)

            if isTest:
                print('\tdb_row:\t', db_row, '\n')

            # Check number of Artists
            num_artists = db.session.query(Artist).filter(
                Artist.artist_id == artist[ARTIST_ID_KEY]).count()
            assert num_artists <= 1

            if num_artists == 1:
                artist_obj = db.session.query(Artist).filter(
                    Artist.artist_id == artist[ARTIST_ID_KEY]).first()

                if isTest:
                    print('Updating an artist entry')
                    print('Before update', artist_obj.serialize())

                # sadly using .update(db_row) produces an error I'm not sure how to fix
                artist_obj.artist_name = db_row['artist_name']
                artist_obj.follower_count = db_row['follower_count']
                artist_obj.genre = db_row['genre']
                artist_obj.popularity = db_row['popularity']
                artist_obj.image_url = db_row['image_url']

                if isTest:
                    print('After update', artist_obj.serialize())

            else:
                a = Artist(**db_row)
                db.session.add(a)

        db.session.commit()


# Populate the database with albums
def populate_albums(populatedArtist, populatedSong, isTest=False):
    json_file = JSON_TEST_ALBUM if isTest else JSON_ALBUM
    with open(json_file) as jsn:
        album_data = json.load(jsn)

        if isTest:
            print('\talbum_data:\t', album_data, '\n')

        for album in album_data:
            if isTest:
                print('\talbum:\t', album, '\n')

            # col names come from db table defn and should not be changed!
            db_row = dict({
                'album_id': album[ALBUM_ID_KEY],
                'album_name': album[ALBUM_NAME_KEY],
                'artist': [],   # fill later
                'song_count': int(album[ALBUM_TRACK_COUNT_KEY]),
                'release_date': album[ALBUM_RELEASE_DATE_KEY],
                'label': album[ALBUM_LABEL_KEY],
                # 'popularity'    : int(album[POPULARITY_KEY]),
                'image_url': album[ALBUM_IMAGE_URL_KEY],
                'is_explicit': album[ALBUM_EXPLICIT_KEY]
            })

            # link up any existing artists or add a temp placeholder entry
            # need to add the object for some reason
            artist_dict = dict(album[ARTIST_KEY])
            for a_name, a_id in artist_dict.items():

                # artist_id is the PK so if we have multiple entries, we're in trouble
                num_artists = db.session.query(Artist).filter(
                    Artist.artist_id == a_id).count()
                assert num_artists <= 1

                if num_artists == 1:
                    artist_obj = db.session.query(Artist).filter(
                        Artist.artist_id == a_id).first()
                    db_row['artist'].append(artist_obj)
                else:
                    artist_obj = Artist(artist_id=a_id, artist_name=a_name)
                    db_row['artist'].append(artist_obj)

            num_albums = db.session.query(Album).filter(
                Album.album_id == album[ALBUM_ID_KEY]).count()
            assert num_albums <= 1

            if num_albums == 1:
                album_obj = db.session.query(Album).filter(
                    Album.album_id == album[ALBUM_ID_KEY]).first()

                if isTest:
                    print('Updating an album entry')
                    print('Before update', album_obj.serialize())

                # sadly using .update(db_row) produces an error I'm not sure how to fix
                album_obj.album_name = db_row['album_name']
                album_obj.artist = db_row['artist']
                album_obj.song_count = db_row['song_count']
                album_obj.release_date = db_row['release_date']
                album_obj.label = db_row['label']
                #album_obj.popularity        = db_row['popularity']
                album_obj.image_url = db_row['image_url']
                album_obj.is_explicit = db_row['is_explicit']

                if isTest:
                    print('After update', album_obj.serialize())

            else:
                a = Album(**db_row)
                db.session.add(a)

        db.session.commit()


# Populate the database with songs
def populate_songs(populatedArtist, populatedAlbum, isTest=False):
    json_file = JSON_TEST_SONG if isTest else JSON_SONG
    with open(json_file) as jsn:
        song_data = json.load(jsn)

        if isTest:
            print('\tsong_data:\t', song_data, '\n')

        for song in song_data:
            if isTest:
                print('\tsong:\t', song, '\n')

            # col names come for db table defn and should not be changed!
            db_row = dict({
                'song_id': song[SONG_ID_KEY],
                'song_name': song[SONG_NAME_KEY],
                'artist': [],   # fill later
                'album': song[ALBUM_ID_KEY],
                'duration': song[DURATION_KEY],
                'is_explicit': bool(song[IS_EXPLICIT_KEY]),
                'track_no': int(song[TRACK_NO_KEY]),
                'popularity': int(song[POPULARITY_KEY])
            })

            # link up any existing artists or add a temp placeholder entry
            # need to add the object for some reason
            artist_dict = dict(song[ARTIST_KEY])
            for a_name, a_id in artist_dict.items():

                # artist_id is the PK so if we have multiple entries, we're in trouble
                num_artists = db.session.query(Artist).filter(
                    Artist.artist_id == a_id).count()
                assert num_artists <= 1

                if num_artists == 1:
                    artist_obj = db.session.query(Artist).filter(
                        Artist.artist_id == a_id).first()
                    db_row['artist'].append(artist_obj)
                else:
                    artist_obj = Artist(artist_id=a_id, artist_name=a_name)
                    db_row['artist'].append(artist_obj)

            # likewise for album, add a place holder if album does not exist
            # needs to be the album PK (as FK) for some reason
            # album_id is the PK so if we have multiple entries, we're in trouble
            num_albums = db.session.query(Album).filter(
                Album.album_id == song[ALBUM_ID_KEY]).count()
            assert num_albums <= 1

            if num_albums == 0:
                album_obj = Album(album_id=song[ALBUM_ID_KEY], album_name=song[ALBUM_NAME_KEY],
                                  release_date=song[RELEASE_DATE_KEY], image_url=song[SONG_IMAGE_URL_KEY])
                db.session.add(album_obj)

            if isTest:
                print('\tdb_row:\t', db_row, '\n')

            num_songs = db.session.query(Song).filter(
                Song.song_id == song[SONG_ID_KEY]).count()
            assert num_songs <= 1

            if num_songs == 1:
                song_obj = db.session.query(Song).filter(
                    Song.song_id == song[SONG_ID_KEY]).first()

                if isTest:
                    print('Updating a song entry')
                    print('Before update', song_obj.serialize())

                # sadly using .update(db_row) produces an error I'm not sure how to fix
                song_obj.song_name = db_row['song_name']
                song_obj.artist = db_row['artist']
                song_obj.album = db_row['album']
                song_obj.duration = db_row['duration']
                song_obj.is_explicit = db_row['is_explicit']
                song_obj.track_no = db_row['track_no']
                song_obj.popularity = db_row['popularity']

                if isTest:
                    print('After update', song_obj.serialize())

            else:
                s = Song(**db_row)
                db.session.add(s)

        db.session.commit()


if __name__ == "__main__":  # pragma: no cover
    reset_db()
    populate_db()
