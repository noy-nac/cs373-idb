from cProfile import label
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
import os

# Import global variables (password for database)
from Global import DB_PASS

# Database variables
USER = "postgres"
PASSWORD = DB_PASS
PUBLIC_IP_ADDRESS = "localhost:5432"
DBNAME = "htmelodiesdb"

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = \
    os.environ.get(
        "DB_STRING", f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')

# To suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

# GCP Deployment and database
cors = CORS(app)
db = SQLAlchemy(app)


# Relation tables
album_artist_link = db.Table('album_artist_link',
                             db.Column('album_id', db.String(),
                                       db.ForeignKey('album.album_id')),
                             db.Column('artist_id', db.String(),
                                       db.ForeignKey('artist.artist_id'))
                             )

artist_genre_link = db.Table('artist_genre_link',
                             db.Column('artist_id', db.String(),
                                       db.ForeignKey('artist.artist_id')),
                             db.Column('genre_id', db.String(),
                                       db.ForeignKey('genre.genre_id'))
                             )

song_artist_link = db.Table('song_artist_link',
                            db.Column('song_id', db.String(),
                                      db.ForeignKey('song.song_id')),
                            db.Column('artist_id', db.String(),
                                      db.ForeignKey('artist.artist_id'))
                            )


class Artist(db.Model):
    __tablename__ = 'artist'

    artist_id = db.Column(db.String(), primary_key=True)
    artist_name = db.Column(db.String(), nullable=False)
    #album_count         = db.Column(db.Integer)
    follower_count = db.Column(db.Integer)
    genre = db.relationship(
        'Genre', secondary='artist_genre_link', backref='has_music_style')  # Many-Many
    #is_solo             = db.Column(db.Boolean)
    #origin              = db.Column(db.String(80))
    #monthly_listeners   = db.Column(db.Integer)
    # convert this to Low, Med, High in backend api layer
    popularity = db.Column(db.Integer)
    image_url = db.Column(db.String())

    # calculated attributes: album_count, song_count

    def serialize(self, withAlbums=False):
        serialObj = dict({
            'artist_id': self.artist_id,
            'artist_name': self.artist_name,
            'follower_count': self.follower_count,
            'genre': [g.genre_id for g in self.genre],
            'popularity': self.popularity,
            'image_url': self.image_url
        })

        if withAlbums:
            serialObj.update({'albums': [a.album_id for a in (
                db.session.query(Album).filter_by(artist=self.artist_id).all())]})

        return serialObj


class Album(db.Model):
    __tablename__ = 'album'

    album_id = db.Column(db.String(), primary_key=True)
    album_name = db.Column(db.String(), nullable=False)
    artist = db.relationship(
        'Artist', secondary='album_artist_link', backref='album_recorded_by')  # Many-Many
    song_count = db.Column(db.Integer)
    release_date = db.Column(db.String(20))
    label = db.Column(db.String(80))
    #language            = db.Column(db.String(40))
    #duration            = db.Column(db.String(20))
    # popularity          = db.Column(db.Integer) # convert this to Low, Med, High in backend api layer
    image_url = db.Column(db.String())
    is_explicit = db.Column(db.Boolean)

    # calculate attributes: duration;

    songs = db.relationship('Song', backref='songs_in_album')

    def serialize(self, withSongs=False):
        serialObj = dict({
            'album_id': self.album_id,
            'album_name': self.album_name,
            'artist': [a.artist_id for a in self.artist],
            'song_count': self.song_count,
            'release_date': self.release_date,
            'label': self.label,
            # 'popularity'        : self.popularity,
            'image_url': self.image_url,
            'is_explicit': self.is_explicit
        })

        if withSongs:
            serialObj.update({'songs': [s.song_id for s in self.songs]})

        return serialObj


class Song(db.Model):
    __tablename__ = 'song'

    song_id = db.Column(db.String(), primary_key=True)
    song_name = db.Column(db.String(), nullable=False)
    artist = db.relationship(
        'Artist', secondary='song_artist_link', backref='song_recorded_by')  # Many-Many
    album = db.Column(db.String(), db.ForeignKey('album.album_id'))
    #release_date        = db.Column(db.String(20))
    #language            = db.Column(db.String(40))
    duration = db.Column(db.String(20))
    is_explicit = db.Column(db.Boolean)
    track_no = db.Column(db.Integer)
    # convert this to Low, Med, High in backend api layer
    popularity = db.Column(db.Integer)

    # calculate attributes: release_date, image_url

    def serialize(self):
        serialObj = dict({
            'song_id': self.song_id,
            'song_name': self.song_name,
            'artist': [a.artist_id for a in self.artist],
            'album': self.album,
            'duration': self.duration,
            'is_explicit': self.is_explicit,
            'track_no': self.track_no,
            'popularity': self.popularity
        })
        return serialObj


# Need to have a table for the genre list
class Genre(db.Model):
    __tablename__ = 'genre'

    genre_id = db.Column(db.String(), primary_key=True)
    # genre_name          = db.Column(db.String(40), nullable=False) # same thing as id actually lol


if __name__ == "__main__":
    print(db)
