from itertools import count
import json
import math
from tokenize import group
from flask import jsonify, request
from subprocess import Popen, PIPE
from db import app, db, Song, Album, Artist, reset_db, populate_db
from sqlalchemy import func  # BRING THE FUNC
import operator
from datetime import *
import re


# Data for each page
about_page = {}
output_page = {}
songs_data = []
albums_data = []
artists_data = []


# Data for visualization testing
games_data = []
genres_data = []
companies_data = []

with open('data/game_data.json') as json_file:
    games_data = json.load(json_file)

with open('data/genre_data.json') as json_file:
    genres_data = json.load(json_file)

with open('data/company_data.json') as json_file:
    companies_data = json.load(json_file)


# Routes for visualization testing
@app.route('/games/')
def games():
    return jsonify({"Games": games_data})


@app.route('/genres/')
def genres():
    return jsonify({"Genres": genres_data})


@app.route('/companies/')
def companies():
    return jsonify({"Companies": companies_data})


# Our routes
@app.route('/')
def index():
    return "Change endpoint to see JSON"


@app.route('/about/')
def about_path():  # pragma: no cover
    return jsonify(about())


def about():
    about_page = {}
    with open('data/about.json') as json_file:
        about_page = json.load(json_file)
    return about_page


# Runs filename from about page: db_tests, api_tests
@app.route('/output/<filename>')
def output(filename):  # pragma: no cover
    p = Popen(["coverage", "run", filename + ".py"], stderr=PIPE, stdout=PIPE)
    res = p.communicate()
    decoded_res = []
    for arr in res:
        if arr is not None:
            decoded = arr.decode('utf-8')
            decoded_res.append(decoded)
    output_page = {"Output": decoded_res}
    return jsonify(output_page)


@app.route('/song/<id>')
def song_path(id):  # pragma: no cover
    return jsonify(song(id))


@app.route('/album/<id>')
def album_path(id):  # pragma: no cover
    return jsonify(album(id))


@app.route('/artist/<id>')
def artist_path(id):  # pragma: no cover
    return jsonify(artist(id))


@app.route('/songs/',  methods=['GET'])
def songs_path():  # pragma: no cover
    vars = dict({"songText": "", "artistText": "", "albumText": "", "releasedFrom": "",
                "releasedTo": "", "durationMin": "", "durationMax": "", "hideExplicit": ""})
    for var in vars.keys():
        vars[var] = request.args.get(var)
        if vars[var] != None:
            vars[var] = request.args[var]
        else:
            vars[var] = ""
    song_output = filter_songs(songText=vars["songText"], artistText=vars["artistText"], albumText=vars["albumText"], releasedFrom=vars["releasedFrom"],
                               releasedTo=vars["releasedTo"], durationMin=vars["durationMin"], durationMax=vars["durationMax"], hideExplicit=vars["hideExplicit"])
    songs, count = pagination(song_output)
    return jsonify({"Songs": songs, "Count": count})

# Paginate list based on params from URL
# def test(query_list):
#     group_size = request.args.get("groupSize", type=int)
#     # page_size = 10
#     count = len(query_list)
#     result = {}
#     pages = math.ceil(count / group_size)
#     for i in range(pages):
#         if i + group_size <= count:
#             result[i + 1] = query_list[i: i+group_size]
#         else:
#             result[i + 1] = query_list[i:]
#     return result, len(result)


@app.route('/albums/', methods=['GET'])
def albums_path():  # pragma: no cover
    vars = dict({"albumText": "", "artistText": "", "labelText": "", "releasedFrom": "",
                "releasedTo": "", "songCountMin": "", "songCountMax": "", "hideExplicit": ""})
    for var in vars.keys():
        vars[var] = request.args.get(var)
        if vars[var] != None:
            vars[var] = request.args[var]
        else:
            vars[var] = ""
    album_output = filter_albums(albumText=vars['albumText'], artistText=vars['artistText'], labelText=vars['labelText'], releasedFrom=vars['releasedFrom'],
                                 releasedTo=vars['releasedTo'], songCountMin=vars['songCountMin'], songCountMax=vars['songCountMax'], hideExplicit=vars["hideExplicit"])
    albums, count = pagination(album_output)
    return jsonify({"Albums": albums, "Count": count})


@app.route('/artists/', methods=['GET'])
def artists_path():  # pragma: no cover
    vars = dict({"artistText": "", "genreText": "", "labelText": "",
                "rankMin": "", "rankMax": "", "monthlyMin": "", "monthlyMax": ""})
    for var in vars.keys():
        vars[var] = request.args.get(var)
        if vars[var] != None:
            vars[var] = request.args[var]
        else:
            vars[var] = ""
    artist_output = filter_artists(artistText=vars['artistText'], genreText=vars['genreText'], rankMin=vars['rankMin'],
                                   rankMax=vars['rankMax'], monthlyMin=vars['monthlyMin'], monthlyMax=vars['monthlyMax'])
    artists, count = pagination(artist_output)
    return jsonify({"Artists": artists, "Count": count})


# Query the database for a Song given its id
def song(id):
    q = db.session.query(Song).filter(Song.song_id == id).first()
    q = q.serialize()
    artist_vals = []
    for a in q['artist']:

        # again BRUH
        artist_obj = db.session.query(Artist).filter(Artist.artist_id == a).first().serialize()#artist(a)

        artist_dict = {'artist_id': artist_obj['artist_id'], 'artist_name': artist_obj['artist_name']}
        artist_vals.append(artist_dict)

    # BRUH ... making multiple calls to other endpoints is killing performance
    album_obj = db.session.query(Album).filter(Album.album_id == q['album']).first().serialize()#album(q['album'])

    output = dict({
        'song_name': q['song_name'],
        'popularity': q['popularity'],
        'song_id': q['song_id'],
        'is_explicit': q['is_explicit'],
        'track_no': q['track_no'],
        'album_name': album_obj['album_name'],
        'album_id': q['album'],
        'release_date': album_obj['release_date'][6:] + "-" + album_obj['release_date'][:2] + "-" + album_obj['release_date'][3:5],
        'duration': q['duration'],
        'image_url': album_obj['image_url'],
        'artist': artist_vals
    })
    return output


# Query the database for an Album given its id
def album(id):
    q = db.session.query(Album).filter(Album.album_id == id).first()
    q = q.serialize()
    artist_vals = []
    for a in q['artist']:

        # again BRUH
        artist_obj = db.session.query(Artist).filter(Artist.artist_id == a).first().serialize()#artist(a)

        artist_dict = {'artist_id': artist_obj['artist_id'], 'artist_name': artist_obj['artist_name']}
        artist_vals.append(artist_dict)

    output = dict({
        "album_name": q['album_name'],
        "album_id": q['album_id'],
        "artist": artist_vals,
        "image_url": q['image_url'],
        "release_date": q['release_date'],
        "label": q['label'],
        "song_count": q['song_count'],
        "is_explicit": q['is_explicit']
    })
    return output


# Query the database for an Artist given its id
def artist(id):
    num_records = db.session.query(Artist).filter(
        Artist.artist_id == id).count()
    if num_records == 0:
        return None
    else:
        q = db.session.query(Artist).filter(Artist.artist_id == id).first()
        q = q.serialize()
        output = dict({
            "artist_name": q['artist_name'],
            "artist_id": q['artist_id'],
            "follower_count": q['follower_count'],
            "genre": q['genre'],
            "popularity": q['popularity'],
            "image_url": q['image_url']
        })
        return output


# Paginate list based on params from URL
def pagination(query_list):
    page = request.args.get("page", type=int)
    # page_size = request.args.get("pagesize", type=int)
    page_size = 10
    count = len(query_list)
    result = query_list
    if page != None:
        index = (page - 1) * page_size
        result = query_list[index: index + page_size]
        if count < (index + page_size):
            result = query_list[index:]
    return result, len(result)

# Paginate list based on params from URL
def pagination_args(query_list, page, page_size):
    count = len(query_list)
    result = query_list
    if page != None and page > 0:
        index = (page - 1) * page_size
        if index > count:
            result = []
        elif count < (index + page_size):
            result = query_list[index:]
        else:
            result = query_list[index: index + page_size]
        
    return result, len(result)

# Query all Songs
def all_songs():
    query = db.session.query(Song).all()
    output = []
    for q in query:
        q = q.serialize()
        artist_vals = []
        for a in q['artist']:
            artist_obj = db.session.query(Artist).filter(Artist.artist_id == a).first().serialize()#artist(a)

            artist_dict = {'artist_id': artist_obj['artist_id'], 'artist_name': artist_obj['artist_name']}
            artist_vals.append(artist_dict)

        album_obj = db.session.query(Album).filter(Album.album_id == q['album']).first().serialize()#album(q['album'])

        temp = dict({
            'song_name': q['song_name'],
            'popularity': q['popularity'],
            'song_id': q['song_id'],
            'is_explicit': q['is_explicit'],
            'track_no': q['track_no'],
            'album_name': album_obj['album_name'],
            'album_id': q['album'],
            'release_date': album_obj['release_date'][6:] + "-" + album_obj['release_date'][:2] + "-" + album_obj['release_date'][3:5],
            'duration': q['duration'],
            'image_url': album_obj['image_url'],
            'artist': artist_vals
        })
        output.append(temp)
    return output


# Query all Albums
def all_albums():
    query = db.session.query(Album).all()
    output = []
    for q in query:
        q = q.serialize()
        artist_vals = []
        for a in q['artist']:
            artist_obj = db.session.query(Artist).filter(Artist.artist_id == a).first().serialize()#artist(a)

            artist_dict = {'artist_id': artist_obj['artist_id'], 'artist_name': artist_obj['artist_name']}
            artist_vals.append(artist_dict)
        temp = dict({
            "album_name": q['album_name'],
            "album_id": q['album_id'],
            "artist": artist_vals,
            "image_url": q['image_url'],
            "release_date": q['release_date'],
            "label": q['label'],
            "song_count": q['song_count'],
            "is_explicit": q['is_explicit']
        })
        output.append(temp)
    return output


# Query all Artists
def all_artists():
    query = db.session.query(Artist).all()
    output = []
    for q in query:
        q = q.serialize()
        temp = dict({
            "artist_name": q['artist_name'],
            "artist_id": q['artist_id'],
            "follower_count": q['follower_count'],
            "genre": q['genre'],
            "popularity": q['popularity'],
            "image_url": q['image_url']
        })
        output.append(temp)
    return output


@app.route('/search/', methods=['GET'])
def search_path():

    if request.args.get('q') is None:
        return jsonify({"Search": []})

    search_string = request.args['q']
    # search_string = "wasted on you"  # TODO: Get search_string from frontend
    return jsonify({"Search": all_search(search_string)})


# Global search for songs, albums, and artists
def all_search(search_string):
    added_songs = []  # keeps track of the added songs to prevent duplicates

    # search for perfect match first (whole string should match)
    final_output, added_songs = search(added_songs, search_string)

    search_list = search_string.split()
    # now search for matches in individual words in the search string
    for search_word in search_list:
        print(len(added_songs))
        cur_output, added_songs = search(added_songs, search_word)
        final_output = [*final_output, *cur_output]
    return final_output


# Helper method for all_search()
def search(added_songs, search_word):
    new_added_songs = added_songs
    output = []
    query = db.session.query(Song).all()
    for song in query:
        song = song.serialize()
        if song['song_id'] not in new_added_songs:
            hit = 0
            # check if search parameter is in song name
            if search_word.lower() in song['song_name'].lower():
                song['song_name'] = highlight(song['song_name'], [search_word])
                if hit < 5:
                    hit = 5

            # check if search parameter is in album name
            album_name = db.session.query(Album).filter(
                Album.album_id == song['album']).first().serialize()['album_name']
            if search_word.lower() in album_name.lower():
                album_name = highlight(album_name, [search_word])
                if hit < 3:
                    hit = 3

            # check if search parameter is in album label name
            label = db.session.query(Album).filter(
                Album.album_id == song['album']).first().serialize()['label']
            if search_word.lower() in label.lower():
                label = highlight(label, [search_word])
                if hit < 2:
                    hit = 2

            all_genre = []
            all_artist = []
            # check if search parameter is in artist name
            for artist in song['artist']:
                artist_obj = db.session.query(Artist).filter(
                    Artist.artist_id == artist).first().serialize()
                cur_artist = artist_obj['artist_name']
                # canyon wuz here to change the json output for frontend
                if search_word.lower() in cur_artist.lower():
                    cur_artist = highlight(cur_artist, [search_word])
                    if hit < 4:
                        hit = 4

                all_artist.append(
                    {'artist_id': artist, 'artist_name': cur_artist, 'image_url': artist_obj['image_url']})

                # check if search parameter is in artist's list of genres
                cur_genres = db.session.query(Artist).filter(
                    Artist.artist_id == artist).first().serialize()['genre']
                for genre in cur_genres:
                    if search_word.lower() in genre.lower():
                        genre = highlight(genre, [search_word])
                        if hit < 1:
                            hit = 1
                    if genre not in all_genre:
                        all_genre.append(genre)

            # return object format
            temp = dict({
                'hit': hit,
                'song_name': song['song_name'],
                'song_id': song['song_id'],
                'is_explicit': song['is_explicit'],
                'track_no': song['track_no'],
                'album_name': album_name,
                'album_id': song['album'],
                'image_url': db.session.query(Album).get(song['album']).image_url,
                'artist': all_artist,
                'label': label,
                'genre': all_genre
            })
            # add the song to added list and output if there was any hit
            if hit != 0:
                new_added_songs.append(song['song_id'])
                output.append(temp)

    # sort the output by hit priority
    output.sort(key=operator.itemgetter('hit'), reverse=True)
    return output, new_added_songs


def highlight(text, keywords):
    def replacement(match): return re.sub(
        r'([^\s]+)', r'<mark>\1</mark>', match.group())
    new_text = re.sub("|".join(map(re.escape, keywords)),
                      replacement, text, flags=re.I)
    return new_text


# Local filter for songs
def filter_songs(songText="", artistText="", albumText="", releasedFrom="", releasedTo="", durationMin="", durationMax="", min_popularity="", max_popularity="", hideExplicit=""):
    # start with all song_ids
    song_ids = set()
    query = db.session.query(Song).all()
    for q in query:
        q = q.serialize()
        song_ids.add(q['song_id'])

    # check each filter and remove songs from song_ids that do not fit the condition
    if songText != "":
        query = db.session.query(Song).filter(func.lower(
            Song.song_name).contains(songText.lower())).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)
    if albumText != "" or releasedFrom != "" or releasedTo != "":
        albums = filter_albums(
            albumText=albumText, releasedFrom=releasedFrom, releasedTo=releasedTo)
        album_ids = set()
        for a in albums:
            album_ids.add(a['album_id'])
        temp = set()
        for song_id in song_ids:
            if song(song_id)['album_id'] in album_ids:
                temp.add(song_id)
        song_ids = song_ids.intersection(temp)
    if artistText != "":
        artists = filter_artists(artistText=artistText)
        artist_ids = set()
        for a in artists:
            artist_ids.add(a['artist_id'])
        temp = set()
        for song_id in song_ids:
            if song(song_id)['artist'][0]['artist_id'] in artist_ids:
                temp.add(song_id)
        song_ids = song_ids.intersection(temp)
    if min_popularity != "":
        query = db.session.query(Song).filter(
            Song.popularity >= min_popularity).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)
    if max_popularity != "":
        query = db.session.query(Song).filter(
            Song.popularity <= max_popularity).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)
    if hideExplicit.lower() == "true":
        query = db.session.query(Song).filter(
            Song.is_explicit == 'false').all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)
    if durationMin != "":
        query = db.session.query(Song).filter(
            Song.duration >= durationMin).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)
    if durationMax != "":
        query = db.session.query(Song).filter(
            Song.duration <= durationMax).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['song_id'])
        song_ids = song_ids.intersection(temp)

    # get the song data for the songs that meet all criteria
    output = []
    for song_id in song_ids:
        output.append(song(song_id))
    return output


# Local filter for albums
def filter_albums(albumText="", artistText="", labelText="", releasedFrom="", releasedTo="", songCountMin="", songCountMax="", hideExplicit=""):
    album_ids = set()
    query = db.session.query(Album).all()
    for q in query:
        q = q.serialize()
        album_ids.add(q['album_id'])

    if albumText != "":
        query = db.session.query(Album).filter(func.lower(
            Album.album_name).contains(albumText.lower())).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['album_id'])
        album_ids = album_ids.intersection(temp)
    if artistText != "":
        artists = filter_artists(artistText=artistText)
        artist_ids = set()
        for a in artists:
            artist_ids.add(a['artist_id'])
        temp = set()
        for album_id in album_ids:
            if album(album_id)['artist'][0]['artist_id'] in artist_ids:
                temp.add(album_id)
        album_ids = album_ids.intersection(temp)
    if labelText != "":
        query = db.session.query(Album).filter(func.lower(
            Album.label).contains(labelText.lower())).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['album_id'])
        album_ids = album_ids.intersection(temp)
    if releasedFrom != "":
        releasedFrom = date(int(releasedFrom[0:4]), int(
            releasedFrom[5:7]), int(releasedFrom[8:10]))
    if releasedTo != "":
        releasedTo = date(int(releasedTo[0:4]), int(
            releasedTo[5:7]), int(releasedTo[8:10]))

    if releasedFrom != "" or releasedTo != "":
        query = db.session.query(Album).all()
        for q in query:
            q = q.serialize()

            # handle case where album release date does not exist
            if q['release_date'] != '0':
                album_release_date = date(int(q['release_date'][6:10]), int(
                    q['release_date'][0:2]), int(q['release_date'][3:5]))
                if releasedFrom != "" and releasedFrom > album_release_date:
                    album_ids.discard(q['album_id'])
                elif releasedTo != "" and releasedTo < album_release_date:
                    album_ids.discard(q['album_id'])
            else:
                album_ids.discard(q['album_id'])
    if songCountMin != "":
        query = db.session.query(Album).filter(
            Album.song_count >= songCountMin).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['album_id'])
        album_ids = album_ids.intersection(temp)

    if songCountMax != "":
        query = db.session.query(Album).filter(
            Album.song_count <= songCountMax).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['album_id'])
        album_ids = album_ids.intersection(temp)
    if hideExplicit.lower() == "true":
        query = db.session.query(Album).filter(
            Album.is_explicit == 'false').all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['album_id'])
        album_ids = album_ids.intersection(temp)

    output = []
    for album_id in album_ids:
        output.append(album(album_id))
    return output


# Local filter for artists
def filter_artists(artistText="", genreText="", rankMin="", rankMax="", monthlyMin="", monthlyMax=""):
    artist_ids = set()
    query = db.session.query(Artist).all()
    for q in query:
        q = q.serialize()
        artist_ids.add(q['artist_id'])

    if artistText != "":
        query = db.session.query(Artist).filter(func.lower(
            Artist.artist_name).contains(artistText.lower())).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)
    if genreText != "":
        query = db.session.query(Artist).all()
        temp = set()
        for q in query:
            q = q.serialize()
            for g in q['genre']:
                if genreText in g:
                    temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)

    if rankMin != "":
        query = db.session.query(Artist).filter(
            Artist.popularity >= rankMin).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)
    if rankMax != "":
        query = db.session.query(Artist).filter(
            Artist.popularity <= rankMax).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)

    if monthlyMin != "":
        query = db.session.query(Artist).filter(
            Artist.follower_count >= monthlyMin).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)
    if monthlyMax != "":
        query = db.session.query(Artist).filter(
            Artist.follower_count <= monthlyMax).all()
        temp = set()
        for q in query:
            q = q.serialize()
            temp.add(q['artist_id'])
        artist_ids = artist_ids.intersection(temp)

    output = []
    for artist_id in artist_ids:
        output.append(artist(artist_id))
    return output


reset_db()
populate_db()

# Set up the database then deploy app
if __name__ == "__main__":  # pragma: no cover
    reset_db()
    populate_db()
    app.run(debug=True)     # don't use debug in prod?
