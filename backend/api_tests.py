import json
from unittest import main, TestCase
from db import reset_db, populate_db
from main import index, about, song, album, artist, all_songs, all_albums, all_artists, filter_songs, filter_albums, filter_artists, pagination_args

with open('data/about.json') as json_file:
    about_page = json.load(json_file)

# Unittests for API


class APITest(TestCase):

    # Simple test of homepage for complete coverage
    def test_index(self):
        result = index()
        self.assertEqual(result, "Change endpoint to see JSON")

    # Simple test of about page for complete coverage
    def test_about(self):
        result = about()
        self.assertEqual(result, about_page)

    # Given a song id, test that our API returns the correct song
    def test_song_by_id_1(self):
        result = song("3F5CgOj3wFlRv51JsHbxhe")
        self.assertEqual(result, {
            "song_name": "Jimmy Cooks (feat. 21 Savage)",
            "popularity": 92,
            "song_id": "3F5CgOj3wFlRv51JsHbxhe",
            "is_explicit": True,
            "track_no": 14,
            "album_name": "Honestly, Nevermind",
            "album_id": "3cf4iSSKd8ffTncbtKljXw",
            'artist': [{'artist_id': '3TVXtAsR1Inumwj472S9r4', 'artist_name': 'Drake'}],
            "release_date": "2022-06-17",
            "duration": "3:38",
            "image_url": "https://i.scdn.co/image/ab67616d0000b2738dc0d801766a5aa6a33cbe37"
        })

    def test_song_by_id_2(self):
        result = song("4LRPiXqCikLlN15c3yImP7")
        self.assertEqual(result, {
            "song_name": "As It Was",
            "popularity": 99,
            "song_id": "4LRPiXqCikLlN15c3yImP7",
            "is_explicit": False,
            "track_no": 1,
            "album_name": "As It Was",
            "album_id": "2pqdSWeJVsXAhHFuVLzuA8",
            'artist': [{'artist_id': '6KImCVD70vtIoJWnq6nGn3', 'artist_name': 'Harry Styles'}],
            "release_date": "2022-03-31",
            "duration": "2:47",
            "image_url": "https://i.scdn.co/image/ab67616d0000b273b46f74097655d7f353caab14"
        })

    # Given an album id, test that our API returns the correct album
    def test_album_by_id_1(self):
        result = album("3cf4iSSKd8ffTncbtKljXw")
        self.assertEqual(result, {
            "album_name": "Honestly, Nevermind",
            "album_id": "3cf4iSSKd8ffTncbtKljXw",
            'artist': [{'artist_id': '3TVXtAsR1Inumwj472S9r4', 'artist_name': 'Drake'}],
            "image_url": "https://i.scdn.co/image/ab67616d0000b2738dc0d801766a5aa6a33cbe37",
            "is_explicit": True,
            "label": "OVO",
            "release_date": "06/17/2022",
            "song_count": 14,
        })

    def test_album_by_id_2(self):
        result = album("2pqdSWeJVsXAhHFuVLzuA8")
        self.assertEqual(result, {
            "album_name": "As It Was",
            "album_id": "2pqdSWeJVsXAhHFuVLzuA8",
            'artist': [{'artist_id': '6KImCVD70vtIoJWnq6nGn3', 'artist_name': 'Harry Styles'}],
            "image_url": "https://i.scdn.co/image/ab67616d0000b273b46f74097655d7f353caab14",
            "is_explicit": False,
            "label": "Columbia",
            "release_date": "03/31/2022",
            "song_count": 1,
        })

    # Given an artist id, test that our API returns the correct artist
    def test_artist_by_id_1(self):
        result = artist("3TVXtAsR1Inumwj472S9r4")
        self.assertEqual(result, {
            "artist_name": "Drake",
            "artist_id": "3TVXtAsR1Inumwj472S9r4",
            "follower_count": 64858669,
            "genre": [
                "canadian hip hop",
                "canadian pop",
                "hip hop",
                "rap",
                "toronto rap"
            ],
            "image_url": "https://i.scdn.co/image/ab6761610000e5eb4293385d324db8558179afd9",
            "popularity": 95
        })

    def test_artist_by_id_2(self):
        result = artist("6KImCVD70vtIoJWnq6nGn3")
        self.assertEqual(result, {
            "artist_name": "Harry Styles",
            "artist_id": "6KImCVD70vtIoJWnq6nGn3",
            "follower_count": 21834251,
            "genre": [
                "pop"
            ],
            "image_url": "https://i.scdn.co/image/ab6761610000e5ebf7db7c8ede90a019c54590bb",
            "popularity": 93
        })

    # Test filtering songs
    def test_filter_songs_1(self):
        result = filter_songs()
        self.assertEqual(sorted(result, key=lambda d: d['song_id']), sorted(
            all_songs(), key=lambda d: d['song_id']))

    def test_filter_songs_2(self):
        result = filter_songs(songText="F.N.F.", releasedFrom='2022-01-01',
                              releasedTo='2022-07-01', durationMin='2:00', durationMax='2:21')
        self.assertEqual(result, [{'song_name': "F.N.F. (Let's Go)", 'popularity': 72, 'song_id': '1vrFJDrysqmsNAgyjBzx4f', 'is_explicit': True, 'track_no': 1, 'album_name': "F.N.F. (Let's Go)", 'album_id': '1FkcZKerCfWg4nUItVHf9B', 'release_date': '2022-05-03',
                         'duration': '2:17', 'image_url': 'https://i.scdn.co/image/ab67616d0000b2730e5c94e31e8c84ecf8eb39bd', 'artist': [{'artist_id': '5pR1zWq3UPsOpW1pTWayLf', 'artist_name': 'Hitkidd'}, {'artist_id': '2qoQgPAilErOKCwE2Y8wOG', 'artist_name': 'GloRilla'}]}])

    def test_filter_songs_3(self):
        result = filter_songs(artistText='lil nas')
        self.assertEqual(sorted(result, key=lambda d: d['song_id']), [{'song_name': 'THATS WHAT I WANT', 'popularity': 90, 'song_id': '0e8nrvls4Qqv5Rfa2UhqmO', 'is_explicit': True, 'track_no': 4, 'album_name': 'MONTERO', 'album_id': '6pOiDiuDQqrmo5DbG0ZubR',
                         'release_date': '2021-09-17', 'duration': '2:23', 'image_url': 'https://i.scdn.co/image/ab67616d0000b273be82673b5f79d9658ec0a9fd', 'artist': [{'artist_id': '7jVv8c5Fj3E9VhNjxT4snq', 'artist_name': 'Lil Nas X'}]}])

    def test_filter_songs_4(self):
        result = filter_songs(albumText='montero')
        self.assertEqual(result, [{'song_name': 'THATS WHAT I WANT', 'popularity': 90, 'song_id': '0e8nrvls4Qqv5Rfa2UhqmO', 'is_explicit': True, 'track_no': 4, 'album_name': 'MONTERO', 'album_id': '6pOiDiuDQqrmo5DbG0ZubR',
                         'release_date': '2021-09-17', 'duration': '2:23', 'image_url': 'https://i.scdn.co/image/ab67616d0000b273be82673b5f79d9658ec0a9fd', 'artist': [{'artist_id': '7jVv8c5Fj3E9VhNjxT4snq', 'artist_name': 'Lil Nas X'}]}])

    # Test filtering albums
    def test_filter_albums_1(self):
        result = filter_albums(releasedFrom='2022-01-01')
        self.assertLess(len(result), len(all_albums()))

    def test_filter_albums_2(self):
        result = filter_albums(artistText="lil")
        self.assertEqual(sorted(result, key=lambda d: d['album_id']), [{'album_name': 'U-Digg (feat. 42 Dugg & Veeze)', 'album_id': '0gnnmRUd9qg7ihL70KEtVZ', 'artist': [{'artist_id': '5f7VJjfbwm532GiveGC0ZK', 'artist_name': 'Lil Baby'}, {'artist_id': '45gHcnDnMC15sgx3VL7ROG', 'artist_name': '42 Dugg'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b273a2726a1969968cae266444d4', 'release_date': '06/17/2022', 'label': 'Quality Control Music/Motown Records', 'song_count': 1, 'is_explicit': True}, {'album_name': 'Right On', 'album_id': '1dzpQiq9uhCaaeTGFmtn2p', 'artist': [{'artist_id': '5f7VJjfbwm532GiveGC0ZK', 'artist_name': 'Lil Baby'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b2736c6827c3fb94b95e5ff4a4aa', 'release_date': '04/08/2022', 'label': 'Quality Control Music/Motown Records', 'song_count': 1, 'is_explicit': True}, {'album_name': 'In A Minute', 'album_id': '3PZTFPQhr0vHnYGwFUvQco', 'artist': [
                         {'artist_id': '5f7VJjfbwm532GiveGC0ZK', 'artist_name': 'Lil Baby'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b273757a066dcb74f5250be8e48a', 'release_date': '04/07/2022', 'label': 'Quality Control Music/Motown Records', 'song_count': 1, 'is_explicit': True}, {'album_name': '7220 (Reloaded)', 'album_id': '42q2L3R0LUPv3tM1Mabvyl', 'artist': [{'artist_id': '3hcs9uc56yIGFCSy9leWe7', 'artist_name': 'Lil Durk'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b27369ed6f07008b1bb3bcf846dd', 'release_date': '03/18/2022', 'label': 'Alamo', 'song_count': 18, 'is_explicit': True}, {'album_name': 'MONTERO', 'album_id': '6pOiDiuDQqrmo5DbG0ZubR', 'artist': [{'artist_id': '7jVv8c5Fj3E9VhNjxT4snq', 'artist_name': 'Lil Nas X'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b273be82673b5f79d9658ec0a9fd', 'release_date': '09/17/2021', 'label': 'Columbia', 'song_count': 15, 'is_explicit': True}])

    def test_filter_albums_3(self):
        result = filter_albums(labelText="ovo")
        self.assertEqual(result, [{'album_name': 'Honestly, Nevermind', 'album_id': '3cf4iSSKd8ffTncbtKljXw', 'artist': [{'artist_id': '3TVXtAsR1Inumwj472S9r4', 'artist_name': 'Drake'}],
                         'image_url': 'https://i.scdn.co/image/ab67616d0000b2738dc0d801766a5aa6a33cbe37', 'release_date': '06/17/2022', 'label': 'OVO', 'song_count': 14, 'is_explicit': True}])

    def test_filter_albums_4(self):
        result = filter_albums(songCountMin=13, songCountMax=13)
        self.assertEqual(sorted(result, key=lambda d: d['album_id']), [{'album_name': 'Stereotype', 'album_id': '5UgaQfAOaOdfLxFClw8EWa', 'artist': [{'artist_id': '1mfDfLsMxYcOOZkzBxvSVW', 'artist_name': 'Cole Swindell'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b27392264c5483fa95ddf7df26e0', 'release_date': '04/08/2022', 'label': 'Warner Music Nashville', 'song_count': 13, 'is_explicit': False}, {
                         'album_name': "Harry's House", 'album_id': '5r36AJ6VOJtp00oxSkBZ5h', 'artist': [{'artist_id': '6KImCVD70vtIoJWnq6nGn3', 'artist_name': 'Harry Styles'}], 'image_url': 'https://i.scdn.co/image/ab67616d0000b2732e8ed79e177ff6011076f5f0', 'release_date': '05/20/2022', 'label': 'Columbia', 'song_count': 13, 'is_explicit': False}])

    # Test filtering artists
    def test_filter_artists_1(self):
        result = filter_artists(artistText='lil')
        self.assertEqual(sorted(result, key=lambda d: d['artist_name']), [{'artist_name': 'Lil Baby', 'artist_id': '5f7VJjfbwm532GiveGC0ZK', 'follower_count': 10874339, 'genre': ['atl hip hop', 'atl trap', 'rap', 'trap'], 'popularity': 87, 'image_url': 'https://i.scdn.co/image/ab6761610000e5eb2161ef3bab0e5e922a1c297d'}, {'artist_name': 'Lil Durk', 'artist_id': '3hcs9uc56yIGFCSy9leWe7', 'follower_count': 4237420, 'genre': [
                         'chicago drill', 'chicago rap', 'drill', 'hip hop', 'rap', 'trap'], 'popularity': 83, 'image_url': 'https://i.scdn.co/image/ab6761610000e5eb6a61d3a33451c301dbb8edb3'}, {'artist_name': 'Lil Nas X', 'artist_id': '7jVv8c5Fj3E9VhNjxT4snq', 'follower_count': 11349135, 'genre': ['lgbtq+ hip hop', 'pop'], 'popularity': 82, 'image_url': 'https://i.scdn.co/image/ab6761610000e5ebab6bd6e450cbc7629a9a2381'}])

    def test_filter_artists_2(self):
        result = filter_artists(genreText='pop', rankMin=93, rankMax=100)
        self.assertEqual(sorted(result, key=lambda d: d['artist_id']), [{'artist_name': 'Drake', 'artist_id': '3TVXtAsR1Inumwj472S9r4', 'follower_count': 64858669, 'genre': ['canadian hip hop', 'canadian pop', 'hip hop', 'rap', 'toronto rap'], 'popularity': 95, 'image_url': 'https://i.scdn.co/image/ab6761610000e5eb4293385d324db8558179afd9'}, {
                         'artist_name': 'Harry Styles', 'artist_id': '6KImCVD70vtIoJWnq6nGn3', 'follower_count': 21834251, 'genre': ['pop'], 'popularity': 93, 'image_url': 'https://i.scdn.co/image/ab6761610000e5ebf7db7c8ede90a019c54590bb'}])

    def test_filter_artists_3(self):
        result = filter_artists(monthlyMin=1500000, monthlyMax=2500000)
        self.assertEqual(sorted(result, key=lambda d: d['artist_name']), [{'artist_name': 'Cole Swindell', 'artist_id': '1mfDfLsMxYcOOZkzBxvSVW', 'follower_count': 1828553, 'genre': ['contemporary country', 'country', 'country road', 'modern country rock'], 'popularity': 68, 'image_url': 'https://i.scdn.co/image/ab6761610000e5eb03d17f2275f44bd2726efc9e'}, {
                         'artist_name': 'Jack Harlow', 'artist_id': '2LIk90788K0zvyj2JJVwkJ', 'follower_count': 2319697, 'genre': ['deep underground hip hop', 'kentucky hip hop', 'rap'], 'popularity': 85, 'image_url': 'https://i.scdn.co/image/ab6761610000e5ebed3a04c76cbc92b97f59c3c0'}])

    # Given an INVALID artist id, test that our API returns None
    def test_artist_invalid_id(self):
        result = artist("INVALID ARTIST ID")
        self.assertEqual(result, None)

    # Test that our API returns all songs, each song has 11 attributes
    def test_all_songs(self):
        result = all_songs()
        for song in result:
            self.assertEqual(len(song.keys()), 11)

    # Test that our API returns all albums, each album has 8 attributes
    def test_all_albums(self):
        result = all_albums()
        for album in result:
            self.assertEqual(len(album.keys()), 8)

    # Test that our API returns all artists, each artist has 6 attributes
    def test_all_artists(self):
        result = all_artists()
        for artist in result:
            self.assertEqual(len(artist.keys()), 6)

    def test_pagination_1(self):
        result, count = pagination_args(all_songs(), 0, page_size=10)
        self.assertEqual(len(result), count)
        self.assertEqual(count, 100)

    def test_pagination_2(self):
        result, count = pagination_args(all_songs(), 1, page_size=10)
        self.assertEqual(count, 10)

    def test_pagination_3(self):
        result, count = pagination_args(all_songs(), 1, page_size=101)
        self.assertEqual(count, 100)

    def test_pagination_4(self):
        result1, count1 = pagination_args(all_songs(), 1, page_size=15)
        result2, count2 = pagination_args(all_songs(), 2, page_size=15)
        self.assertEqual(count1, count2)
        self.assertNotEqual(result1, result2)

    def test_pagination_5(self):
        result, count = pagination_args(all_songs(), 7, page_size=15)
        self.assertEqual(count, 10)

    def test_pagination_6(self):
        result, count = pagination_args(all_songs(), 11, page_size=10)
        self.assertEqual(count, 0)

if __name__ == "__main__":
    reset_db()
    populate_db()
    main()
