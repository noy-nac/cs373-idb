from unittest import main, TestCase


class MyUnitTests (TestCase) :
    def setUp (self) :
        self.a = [
            theta_join]

        self.r = [
            {"A" : 1, "B" : 4},
            {"A" : 2, "B" : 5},
            {"A" : 3, "B" : 6}]

        self.s = [
            {"C" : 2, "D" : 7},
            {"C" : 3, "D" : 5},
            {"C" : 3, "D" : 6},
            {"C" : 4, "D" : 6}]

    def test_1 (self) :
        for f in self.a :
            with self.subTest() :
                self.assertEqual(
                    list(f(self.r, self.s, lambda u, v : False)),
                    [])

    def test_2 (self) :
        for f in self.a :
            with self.subTest() :
                self.assertEqual(
                    list(f(self.r, self.s, lambda u, v : True)),
                    [{'A': 1, 'B': 4, 'C': 2, 'D': 7},
                     {'A': 1, 'B': 4, 'C': 3, 'D': 5},
                     {'A': 1, 'B': 4, 'C': 3, 'D': 6},
                     {'A': 1, 'B': 4, 'C': 4, 'D': 6},
                     {'A': 2, 'B': 5, 'C': 2, 'D': 7},
                     {'A': 2, 'B': 5, 'C': 3, 'D': 5},
                     {'A': 2, 'B': 5, 'C': 3, 'D': 6},
                     {'A': 2, 'B': 5, 'C': 4, 'D': 6},
                     {'A': 3, 'B': 6, 'C': 2, 'D': 7},
                     {'A': 3, 'B': 6, 'C': 3, 'D': 5},
                     {'A': 3, 'B': 6, 'C': 3, 'D': 6},
                     {'A': 3, 'B': 6, 'C': 4, 'D': 6}])

    def test_3 (self) :
        for f in self.a :
            with self.subTest() :
                self.assertEqual(
                    list(f(self.r, self.s, lambda u, v : u["A"] == v["C"])),
                    [{'A': 2, 'B': 5, 'C': 2, 'D': 7},
                     {'A': 3, 'B': 6, 'C': 3, 'D': 5},
                     {'A': 3, 'B': 6, 'C': 3, 'D': 6}])


def select(table, predicate):

    result = list()

    for row in table:

        if predicate(row):
            result.append(row)

    return result

def select_yield(table, predicate):

    result = list()

    for row in table:

        if predicate(row):
            yield row

def project(table, *cols):

    for row in table:
        
        zapy = dict()

        for key, val in row.items():
        
            if key in cols:
                zapy[key] = val

        yield zapy


def cross_join(t1, t2):

    for row1 in t1:
        for row2 in t2:
            yield dict(row1, **row2)


def theta_join(t1, t2, p):

    for row1 in t1:
        for row2 in t2:
            if p(row1, row2):
                yield dict(row1, **row2)

def natural_join(t1, t2)

if __name__ == "__main__" :
    main()