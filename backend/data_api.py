from asyncio.windows_events import NULL
import requests
import json
from db_secrets import top_100_id
from refresh import Refresh
from db import SONG_ID_KEY, SONG_NAME_KEY, ALBUM_COUNT_KEY, POPULARITY_KEY, \
    SONG_COUNT_KEY, IS_SINGLE_KEY, RELEASE_DATE_KEY, DURATION_KEY, \
    IS_EXPLICIT_KEY, TRACK_NO_KEY, ARTIST_KEY, ARTIST_NAME_KEY, ARTIST_ID_KEY, ALBUM_NAME_KEY, ALBUM_ID_KEY, SONG_ID_KEY, \
    ARTIST_ID_KEY, FOLLOWER_COUNT_KEY, GENRE_KEY, ALBUM_IMAGE_URL_KEY, ARTIST_IMAGE_URL_KEY, ALBUM_RELEASE_DATE_KEY, \
    ALBUM_LABEL_KEY, ALBUM_TRACK_COUNT_KEY, ALBUM_EXPLICIT_KEY
from dateutil import parser


class GetSongsPlaylist:
    # initializing variables
    def __init__(self):
        self.spotify_token = ""
        self.top_100_id = top_100_id

    # get playlist
    def get_playlist(self):
        # querying
        query = "https://api.spotify.com/v1/playlists/{}".format(top_100_id)

        # authenticating using token
        response = requests.get(query,
                                headers={"Content-Type": "application/json",
                                         "Authorization": "Bearer {}".format(self.spotify_token)})

        resp_json = response.json()

        # check to make sure response is valid
        if(str(response) == "<Response [200]>"):

            # formats time into minutes: seconds
            def format_ms(miliseconds):
                miliseconds = int(miliseconds)
                seconds = (miliseconds/1000) % 60
                seconds = int(seconds)
                minutes = (miliseconds/(1000*60)) % 60
                minutes = int(minutes)
                return ("%d:%d" % (minutes, seconds))

            # get the songs and all of its data into a list
            # TODO: new key for album name, album id, and artists
            def get_songs_list():
                songs_list = []
                for item in resp_json['tracks']['items']:
                    curr_song = {}
                    curr_song[SONG_NAME_KEY] = item['track']['name']
                    curr_song[POPULARITY_KEY] = item["track"]["popularity"]
                    curr_song[SONG_ID_KEY] = item["track"]["id"]
                    curr_song[IS_EXPLICIT_KEY] = item["track"]["explicit"]
                    curr_song[TRACK_NO_KEY] = item["track"]["track_number"]
                    curr_song[ALBUM_NAME_KEY] = item['track']['album']['name']
                    curr_song[ALBUM_ID_KEY] = item['track']['album']['id']
                    curr_song[RELEASE_DATE_KEY] = item['track']['album']['release_date']
                    curr_song[DURATION_KEY] = format_ms(
                        item['track']['duration_ms'])
                    curr_song[ALBUM_IMAGE_URL_KEY] = item['track']['album']['images'][0]['url']
                    dict_of_artists = {}
                    for artist in item['track']['album']['artists']:
                        dict_of_artists[artist["name"]] = artist["id"]
                    curr_song[ARTIST_KEY] = dict_of_artists

                    songs_list.append(curr_song)

                json_songs = json.dumps(songs_list)
                f = open("nghia_test_songs.json", "w")
                f.write(json_songs)
                f.close()

            get_songs_list()

    def call_refresh(self):
        print("Refreshing Token")
        refresh_call = Refresh()
        self.spotify_token = refresh_call.refresh()


class GetArtists:
    # initializing variables
    def __init__(self):
        self.spotify_token = ""

    def get_artists(self):
        f = open('nghia_test_songs.json')

        data = json.load(f)

        artists = []
        # Iterating through the json list and get artists/album data
        for song in data:
            for artist in song[ARTIST_KEY]:
                # getting artist name and artist id
                curr_artist = {}
                curr_artist[ARTIST_NAME_KEY] = artist
                curr_artist[ARTIST_ID_KEY] = song[ARTIST_KEY][artist]
                curr_artist[ALBUM_NAME_KEY] = song[ALBUM_NAME_KEY]
                curr_artist[ALBUM_ID_KEY] = song[ALBUM_ID_KEY]
                artists.append(curr_artist)

        # Closing file
        f.close()

        # removing duplicate artists
        # artists_seen = set()
        # artists = []
        # for dict in temp_artists_list:
        #     t = tuple(dict.items())
        #     if t not in artists_seen:
        #         artists_seen.add(t)
        #         artists.append(dict)

        def get_artist_data():
            for artist in artists:

                # querying
                query = "https://api.spotify.com/v1/artists/{}".format(
                    artist[ARTIST_ID_KEY])

                # authenticating using token
                response = requests.get(query,
                                        headers={"Content-Type": "application/json",
                                                 "Authorization": "Bearer {}".format(self.spotify_token)})

                # TODO: make sure response is stable before pulling all the data

                resp_json = response.json()

                # TODO: new key for followers and genres

                artist[FOLLOWER_COUNT_KEY] = resp_json["followers"]["total"]
                artist[GENRE_KEY] = resp_json["genres"]
                artist[POPULARITY_KEY] = resp_json["popularity"]
                artist[ARTIST_IMAGE_URL_KEY] = resp_json["images"][0]["url"]

            json_artists = json.dumps(artists)
            f = open("nghia_test_artists.json", "w")
            f.write(json_artists)
            f.close()

        get_artist_data()

    def call_refresh(self):
        print("Refreshing Token")
        refresh_call = Refresh()
        self.spotify_token = refresh_call.refresh()


class GetAlbums:
    # initializing variables
    def __init__(self):
        self.napster_token = ""

    def get_albums(self):
        f = open('nghia_test_songs.json')

        data = json.load(f)

        albums = []
        # Iterating through the json list and get album/artist data
        for song in data:
            curr_album = {}
            # getting album name and album id
            curr_album[ALBUM_NAME_KEY] = song[ALBUM_NAME_KEY]
            curr_album[ALBUM_ID_KEY] = song[ALBUM_ID_KEY]
            curr_album[ARTIST_KEY] = song[ARTIST_KEY]
            curr_album[ALBUM_IMAGE_URL_KEY] = song[ALBUM_IMAGE_URL_KEY]
            albums.append(curr_album)

        # Closing file
        f.close()

        def get_album_data():
            for album in albums:
                # this is just utterly stupid, but there is no other way to get around napster's internal server error than to requery (davie)
                error = True
                while error:
                    # querying
                    query = "https://api.napster.com/v2.2/search?query={}&type=album&per_type_limit=1".format(
                        album[ALBUM_NAME_KEY])

                    # authenticating using token
                    response = requests.get(
                        query, headers={"apikey": "MTY1ZmRkYTUtMjAwNy00ZGQwLThmMGYtN2IwZjNlNGZmNGZi"})

                    resp_json = response.json()

                    try:
                        if resp_json["meta"]["returnedCount"] == 0:
                            album[ALBUM_RELEASE_DATE_KEY] = NULL
                            album[ALBUM_LABEL_KEY] = NULL
                            album[ALBUM_TRACK_COUNT_KEY] = NULL
                            album[ALBUM_EXPLICIT_KEY] = NULL
                        else:
                            album[ALBUM_RELEASE_DATE_KEY] = parser.parse(
                                resp_json["search"]["data"]["albums"][0]["released"]).strftime("%m/%d/%Y")
                            album[ALBUM_LABEL_KEY] = resp_json["search"]["data"]["albums"][0]["label"]
                            album[ALBUM_TRACK_COUNT_KEY] = resp_json["search"]["data"]["albums"][0]["trackCount"]
                            album[ALBUM_EXPLICIT_KEY] = resp_json["search"]["data"]["albums"][0]["isExplicit"]
                    except KeyError:
                        print("KeyError, too bad! But don't worry.")
                    else:
                        error = False

            json_albums = json.dumps(albums)
            f = open("nghia_test_albums.json", "w")
            f.write(json_albums)
            f.close()

        get_album_data()

    # TODO: refresh token, also confused (davie)


# run the code here
a = GetSongsPlaylist()
b = GetArtists()
c = GetAlbums()
a.call_refresh()
a.get_playlist()
b.call_refresh()
b.get_artists()
c.get_albums()
