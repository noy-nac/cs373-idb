from unittest import main, TestCase
from db import db, Artist, Album, Song, reset_db, populate_db, populate_songs, populate_albums, populate_artists

# Unittests for database


class DBTest(TestCase):

    # Test loading Songs into database
    def test_load_songs_alone(self):
        reset_db()
        populate_songs(populatedArtist=False,
                       populatedAlbum=False, isTest=True)

        query = db.session.query(Song).all()

        for song in query:
            s = song.serialize()
            self.assertEqual(len(s.keys()), 8)
            print(s)

    # Test loading duplicate Songs into database
    def test_load_songs_duplicates(self):
        reset_db()
        populate_songs(populatedArtist=False,
                       populatedAlbum=False, isTest=True)
        populate_songs(populatedArtist=False,
                       populatedAlbum=False, isTest=True)

        query = db.session.query(Song).all()

        for song in query:
            s = song.serialize()
            self.assertEqual(len(s.keys()), 8)
            print(s)

    # Test loading Albums into database
    def test_load_album_alone(self):
        reset_db()
        populate_albums(populatedArtist=False,
                        populatedSong=False, isTest=True)

        query = db.session.query(Album).all()

        for album in query:
            a = album.serialize()
            self.assertEqual(len(a.keys()), 8)
            print(a)

    # Test loading duplicate Albums into database
    def test_load_album_duplicates(self):
        reset_db()
        populate_songs(populatedArtist=False,
                       populatedAlbum=False, isTest=True)
        populate_albums(populatedArtist=False,
                        populatedSong=True, isTest=True)
        populate_albums(populatedArtist=False,
                        populatedSong=True, isTest=True)

        query = db.session.query(Album).all()

        for album in query:
            a = album.serialize()
            self.assertEqual(len(a.keys()), 8)
            print(a)

    # Test loading Artists into database
    def test_load_artists_alone(self):
        reset_db()
        populate_artists(populatedAlbum=False,
                         populatedSongs=False, isTest=True)

        query = db.session.query(Artist).all()

        for artist in query:
            a = artist.serialize()
            self.assertEqual(len(a.keys()), 6)
            print(a)

    # Test loading duplicate Artists into database
    def test_load_artists_duplicates(self):
        reset_db()
        populate_songs(populatedArtist=False,
                       populatedAlbum=False, isTest=True)
        populate_albums(populatedArtist=False,
                        populatedSong=True, isTest=True)
        populate_artists(populatedAlbum=True,
                         populatedSongs=True, isTest=True)
        populate_artists(populatedAlbum=True,
                         populatedSongs=True, isTest=True)

        query = db.session.query(Artist).all()

        for artist in query:
            a = artist.serialize()
            self.assertEqual(len(a.keys()), 6)
            print(a)

    # Test loading Songs, Artists, and Albums into database
    def test_load_all(self):
        reset_db()
        populate_db(isTest=True)

        songs = db.session.query(Song).all()
        albums = db.session.query(Album).all()
        artists = db.session.query(Artist).all()

        for song in songs:
            s = song.serialize()
            self.assertEqual(len(s.keys()), 8)
            print(s)
        for album in albums:
            a = album.serialize()
            self.assertEqual(len(a.keys()), 8)
            print(a)
        for artist in artists:
            a = artist.serialize()
            self.assertEqual(len(a.keys()), 6)
            print(a)


if __name__ == "__main__":
    main()
